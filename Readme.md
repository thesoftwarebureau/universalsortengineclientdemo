# Universal Sort Engines Client Demo

## The Universal Sort Engines Client Demo Application
* The Software Bureau's Universal Sort Engines consists of a suite of classes that provide functionality to sort input address data for the Royal Mail and DSA sorts.
* Also available from The Software Bureau are Universal Sort Engine Clients that simplify harnessing the Universal Sort Engine for writing a sort application.
* There are two flavours of Universal Sort Engine Client
    <ol>
    <li>USEClient where the user needs to take responsibility for file I/O and creating reports from the Universal Sort Engine output streams.</li>
    <li>USEFileClient where the user only need specify input file and output details so that file I/O and reports are taken care off.</li>
    </ol>

* This repo holds a demo application that demonstrates how to make use of the Universal Sort Engine Client's USEFileClient.
* The USEFileClient wraps the USEClient in 600+ lines of code to handle File I/O and reporting.

## Steps Required To Run The Console Application

### Pre-requisite
To build and run this demo application you must have the following
* You must have git installed and a git client such as Git Bash (install from https://git-scm.com/)
* .Net 6.0
* Visual Studio 2022

However as the USEAPIFileClient targets .Net Standard 2.0 it can be used in an application that targets any Windows Framework from 4.7.2 and above.
The USE APIFileClient will not currently run on Linux.

### Step 1 Credentials
* Request NuGet Server credentials and 
* A USE API Product Key

from The Software Bureau Limited at support@thesoftwarebureau.com

### Step 2 Connect Visual Studio to The Software Bureau Nuget Server
* Download latest nuget.exe : https://dist.nuget.org/win-x86-commandline/latest/nuget.exe
* Add TSB as a package feed source from the command line as follows:
	* nuget sources Add -Name "TSB" -Source "https://nuget.thesoftwarebureau.com/nuget"
	* nuget sources Update -Name "TSB" -UserName "UserName" -Password "YOUR_PASSWORD"

![alternativetext](Images/NuGet-Add.png)

* The changes will be saved to your machines configuration file: %appdata%\nuget\NuGet.Config
* Open Visual studio
	* Tools/Options .. NuGet Package Manager
	* You should see TSB listed as a package source

![alternativetext](Images/TSB-As-Packet-Source.png)

### Step 3 Git Pull
* Open Git Bash in your projects directory (install from https://git-scm.com/)
* git clone https://bitbucket.org/thesoftwarebureau/universalsortengineclientdemo.git
* or use your favourite git client to clone the UniversalSortEngineClientDemo solution

![alternativetext](Images/Git-Bash-Clone.png)

### Step 4 Visual Studio
* Open the UniversalSortEngineClientDemo solution in visual studio
* Select the menu option Tools+Nuget Package Manager+Manage Nuget Packages for Solution
* You will see the following packages listed as installed
	* SoftwareBureau.UniversalSortEngine.Data.Resource
    * SoftwareBureau.UniversalSortEngine.XSL.Resource
    * SoftwareBureau.USEAPIFileClient

![alternativetext](Images/Nuget-Packages.png)

* The solutions folder will look like the image below. It will contain
    <ol>
    <li>a "db" folder containing resources added by the SoftwareBureau.UniversalSortEngine.Data.Resource package.</li>
    <li>an "XSL" folder containing the stylesheets that define the reports added by the SoftwareBureau.UniversalSortEngine.XSL.Resource package</li>
    <li>an "Input" folder contains an input.psv file in the format required by the USEFileClient</li>
    <li>a bunch of resources that will be copied to the bin directory on execution</li>
    <li>the SoftwareBureau.UniversalSortEngine.Activate.exe licence activation application</li>
    </ol>

![alternativetext](Images/SolutionsFolder.png)

Please note  
* There will be regular updates to the SoftwareBureau.UniversalSortEngine.Data.Resource package. 
* The XSL stylesheets will largely remain unchanged other than when new DSAs come on stream or when Royal Mail change reporting requirements.

### Step 5 License
* Launce the application SoftwareBureau.UniversalSortEngine.Activate.exe listed as 5. above.
* Press OK in the next window displayed to accept the SystemSettings file

![alternativetext](Images/Settings-File-Accept.png)

* When requested to select Licence Type select "Activation"

![alternativetext](Images/Licence-Type-Activation.png)

* Enter the API Product Key you recieved at Step 1 in the text box and click the Activate button

### Step 6 Run the Application
* A sample input file "input.csv" was supplied with the console application in the Input folder so you are now ready to run the application.
* The application will write the output file and report files to the same Output folder
* Run the console application and the console log will look like this

![alternativetext](Images/Run-Console-Application.png)

* Check the following files in the Output folder

![alternativetext](Images/Output-Files.png)

#### Main Output Files
* Output.txt - main output file
* Output.pdf - the main report
* xyz.LineListing.csv - the linelisting report
* BBC3AAACA.tnt - TNT bag file
* BC3AAACA.bag - simple bag file

#### Additional Output Files
* Output.xml - data main report generated from
* Output.XSL-FO.xml - XSL formatting objects file generated during creation of the pdf report
* Output-Parameters.txt - dump of all parameters used by the sort (defaults as well as specified in program.cs)

#### Application Details to Note
* The input file must be pipe delimited with a header record denoting the fields that must be supplied as follows
A1|A2|A3|A4|A5|A6|A7|Town|Zip_pt1|Zip_pt2|DPS|WEIGHT|OVERSEAS
* When the sort is run the current directory is bin\Debug\net6.0
* On execution the contents of the db and XSL folders are copied to sub-directories of the above folder and the USEFileClient is given these folders within the UseFileClient(string currentDirectory) method as follows
```C#
    var sort = new USEFileClient
    {
        MailSortBinFile = $"{currentDirectory}\\db\\RMSort.bin",
        XslPath = $"{currentDirectory}\\XSL\\usesort-fo.xsl",
    };
```
* The static setting USEFileClient.ControlDirectory is also set in the USEFileClient(string currentDirectory) method

### List of Sort Definitions
#### Citipost High
```C#
    var sortDefinition = new SortDefinition
    {
        Description = "935Citipost",
        Carrier = USEParams.Carriers.Citipost,
        Sorted = true,
        MailMark = false,
        InputFile = InputFile,
        OutputDirectory = testDirectory,
        OutputName = "Output",
        OutputExtension = ".txt",
        Options = new SortParam[]
        {
                    new SortParam<string>(SortParam.DsaSplitType.PropertyName, $"{USEDSAParams.DSASplitTypes.Arbitrage}"),
                    new SortParam<string>(SortParam.Piece.PropertyName, $"{USEParams.Pieces.Letter}"),
                    new SortParam<string>(SortParam.ServiceLevel.PropertyName, $"{USEParams.ServiceLevels.Second}"),
                    new SortParam<string>(SortParam.MailingRef.PropertyName, "#935"),
                    new SortParam<string>(SortParam.Container.PropertyName, $"{USEParams.Containers.Trays}"),
                    new SortParam<string>(SortParam.SortType.PropertyName, $"{USEParams.SortTypes.High}"),
                    new SortParam<string>(SortParam.MailType.PropertyName, $"{USEParams.MailTypes.Business}"),
                    new SortParam<int>(SortParam.Weight.PropertyName, 55),
                    new SortParam<int>(SortParam.MaxContainerContentWeight.PropertyName, 7140),
                    new SortParam<int>(SortParam.MinimumSelectionSize.PropertyName, 25),
                    new SortParam<string>(SortParam.Sustain.PropertyName, $"{USEParams.Sustainabilities.Standard}"),
                    new SortParam<bool>(SortParam.Sort48.PropertyName, false),
                    new SortParam<string>(SortParam.DirectResidueOrder.PropertyName, $"{USEParams.DirectResidueOrders.AlternateDirectResidue}"),
                    new SortParam<bool>(SortParam.AllowSelectionTrayWithdrawal.PropertyName, false),
                    new SortParam<bool>(SortParam.AllowAutoDpsLevelAdjustment.PropertyName, false),
                    new SortParam<bool>(SortParam.SwitchOffVolumeChecking.PropertyName, false),
                    new SortParam<bool>(SortParam.DsaMachinable.PropertyName, false),
        }
    };
```
#### Citipost Low OCR
As above but
* SortType must be USEParams.SortTypes.OCR
* Use the Citipost High Sort definition with MinimumSelectionSize reduced from 25 to 1 and DsaMachinable changed from false to true

#### Citipost Low Manual
As above but
* SortType must be USEParams.SortTypes.Low
* Use the Citipost High Sort definition with MinimumSelectionSize reduced from 25 to 1

#### OnePost High Sort
```C#
    var sortDefinition = new SortDefinition
    {
        Description = "1686OnePost",
        Carrier = USEParams.Carriers.OnePost,
        Sorted = true,
        MailMark = false,
        InputFile = InputFile,
        OutputDirectory = testDirectory,
        OutputName = "Output",
        OutputExtension = ".txt",
        Options = new SortParam[]
        {
            new SortParam<string>(SortParam.DsaSplitType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEDSAParams.DSASplitTypes.Arbitrage}"),
            new SortParam<string>(SortParam.Piece.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Pieces.Letter}"),
            new SortParam<string>(SortParam.ServiceLevel.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.ServiceLevels.Second}"),
            new SortParam<string>(SortParam.MailingRef.PropertyName, "#1686"),
            new SortParam<string>(SortParam.Container.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Containers.Bags}"),
            new SortParam<string>(SortParam.SortType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.SortTypes.High}"),
            new SortParam<string>(SortParam.MailType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.MailTypes.Business}"),
            new SortParam<int>(SortParam.Weight.PropertyName, 55),
            new SortParam<int>(SortParam.MaxContainerContentWeight.PropertyName, 10400),
            new SortParam<int>(SortParam.MinimumSelectionSize.PropertyName, 25),
            new SortParam<string>(SortParam.Sustain.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Sustainabilities.Standard}"),
            new SortParam<bool>(SortParam.Sort48.PropertyName, false),
            new SortParam<string>(SortParam.DirectResidueOrder.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.DirectResidueOrders.AlternateDirectResidue}"),
            new SortParam<bool>(SortParam.AllowSelectionTrayWithdrawal.PropertyName, false),
            new SortParam<bool>(SortParam.AllowAutoDpsLevelAdjustment.PropertyName, false),
            new SortParam<bool>(SortParam.SwitchOffVolumeChecking.PropertyName, false),
            new SortParam<bool>(SortParam.DsaMachinable.PropertyName, false),
        }
    };
```
#### OnePost Low OCR
As above but
* SortType must be USEParams.SortTypes.OCR
* Use the Citipost High Sort definition with DsaMachinable changed from false to true

#### OnePost Low Manual
As above but
* SortType must be USEParams.SortTypes.Low
* Use the Citipost High Sort definition

#### UK Mail High Sort
```C#
    var sortDefinition = new SortDefinition
    {
        Description = "3503UKMail",
        Carrier = USEParams.Carriers.UKMail,
        Sorted = true,
        MailMark = false,
        InputFile = InputFile,
        OutputDirectory = testDirectory,
        OutputName = "Output",
        OutputExtension = ".txt",
        Options = new SortParam[]
        {
            new SortParam<string>(SortParam.DsaSplitType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEDSAParams.DSASplitTypes.National}"),
            new SortParam<string>(SortParam.Piece.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Pieces.Letter}"),
            new SortParam<string>(SortParam.ServiceLevel.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.ServiceLevels.Second}"),
            new SortParam<string>(SortParam.MailingRef.PropertyName, "#3503"),
            new SortParam<string>(SortParam.Container.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Containers.Bags}"),
            new SortParam<string>(SortParam.SortType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.SortTypes.High}"),
            new SortParam<string>(SortParam.MailType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.MailTypes.Business}"),
            new SortParam<int>(SortParam.Weight.PropertyName, 55),
            new SortParam<int>(SortParam.MaxContainerContentWeight.PropertyName, 10800),
            new SortParam<int>(SortParam.MinimumSelectionSize.PropertyName, 25),
            new SortParam<string>(SortParam.Sustain.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Sustainabilities.Standard}"),
            new SortParam<bool>(SortParam.Sort48.PropertyName, false),
            new SortParam<string>(SortParam.DirectResidueOrder.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.DirectResidueOrders.AlternateDirectResidue}"),
            new SortParam<bool>(SortParam.AllowSelectionTrayWithdrawal.PropertyName, false),
            new SortParam<bool>(SortParam.AllowAutoDpsLevelAdjustment.PropertyName, false),
            new SortParam<bool>(SortParam.SwitchOffVolumeChecking.PropertyName, false),
            new SortParam<bool>(SortParam.DsaMachinable.PropertyName, false),
        }
    };
```
#### UK Mail Low OCR
As above but
* SortType must be USEParams.SortTypes.OCR
* Use the Citipost High Sort definition with DsaMachinable changed from false to true

#### UK Mail Low Manual
As above but
* SortType must be USEParams.SortTypes.Low
* Use the Citipost High Sort definition

#### Whistl High Sort
```C#
    var sortDefinition = new SortDefinition
    {
        Description = "4486Whistl",
        Carrier = USEParams.Carriers.Whistl,
        Sorted = true,
        MailMark = false,
        InputFile = InputFile,
        OutputDirectory = testDirectory,
        OutputName = "Output",
        OutputExtension = ".txt",
        Options = new SortParam[]
        {
            new SortParam<string>(SortParam.DsaSplitType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEDSAParams.DSASplitTypes.National}"),
            new SortParam<string>(SortParam.Piece.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Pieces.Letter}"),
            new SortParam<string>(SortParam.ServiceLevel.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.ServiceLevels.Second}"),
            new SortParam<string>(SortParam.MailingRef.PropertyName, "#4486"),
            new SortParam<string>(SortParam.Container.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Containers.Bags}"),
            new SortParam<string>(SortParam.SortType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.SortTypes.High}"),
            new SortParam<string>(SortParam.MailType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.MailTypes.Business}"),
            new SortParam<int>(SortParam.Weight.PropertyName, 55),
            new SortParam<int>(SortParam.MaxContainerContentWeight.PropertyName, 10400),
            new SortParam<int>(SortParam.MinimumSelectionSize.PropertyName, 25),
            new SortParam<string>(SortParam.Sustain.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Sustainabilities.Standard}"),
            new SortParam<bool>(SortParam.Sort48.PropertyName, false),
            new SortParam<string>(SortParam.DirectResidueOrder.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.DirectResidueOrders.AlternateDirectResidue}"),
            new SortParam<bool>(SortParam.AllowSelectionTrayWithdrawal.PropertyName, false),
            new SortParam<bool>(SortParam.AllowAutoDpsLevelAdjustment.PropertyName, false),
            new SortParam<bool>(SortParam.SwitchOffVolumeChecking.PropertyName, false),
            new SortParam<bool>(SortParam.DsaMachinable.PropertyName, false),
        }
    };
```
#### Whistl Low OCR
As above but
* SortType must be USEParams.SortTypes.OCR
* Use the Whistl High Sort definition with DsaMachinable changed from false to true

#### Whistl Low Manual
As above but
* SortType must be USEParams.SortTypes.Low
* Use the Whistl High Sort definition

#### Royal Mail High Sort
```C#
    var sortDefinition = new SortDefinition
    {
        Description = "1RoyalMail",
        Carrier = USEParams.Carriers.RoyalMail,
        Sorted = true,
        MailMark = false,
        InputFile = InputFile,
        OutputDirectory = testDirectory,
        OutputName = "Output",
        OutputExtension = ".txt",
        Options = new SortParam[]
        {
            new SortParam<string>(SortParam.Piece.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Pieces.Letter}"),
            new SortParam<string>(SortParam.ServiceLevel.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.ServiceLevels.Economy}"),
            new SortParam<string>(SortParam.MailingRef.PropertyName, "#1"),
            new SortParam<string>(SortParam.Container.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Containers.Bags}"),
            new SortParam<string>(SortParam.SortType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.SortTypes.High}"),
            new SortParam<string>(SortParam.MailType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.MailTypes.Business}"),
            new SortParam<int>(SortParam.Weight.PropertyName, 55),
            new SortParam<int>(SortParam.MaxContainerContentWeight.PropertyName, 10500),
            new SortParam<int>(SortParam.MinimumSelectionSize.PropertyName, 50),
            new SortParam<string>(SortParam.Sustain.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Sustainabilities.Standard}"),
            new SortParam<bool>(SortParam.Sort48.PropertyName, false),
            new SortParam<string>(SortParam.DirectResidueOrder.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.DirectResidueOrders.AlternateDirectResidue}"),
            new SortParam<bool>(SortParam.AllowSelectionTrayWithdrawal.PropertyName, false),
            new SortParam<bool>(SortParam.AllowAutoDpsLevelAdjustment.PropertyName, false),
            new SortParam<bool>(SortParam.SwitchOffVolumeChecking.PropertyName, false),
        }
    };
```
#### Royal Mail Low OCR
As above but
* SortType must be USEParams.SortTypes.OCR
* Use the Royal Mail High Sort definition with Minimum Selection Size changed from 50 to 1

#### Citipost Mailmark
```C#
    var sortDefinition = new SortDefinition
    {
        Description = "1027CitipostMM",
        Carrier = USEParams.Carriers.Citipost,
        Sorted = true,
        MailMark = true,
        InputFile = InputFile,
        OutputDirectory = testDirectory,
        OutputName = "Output",
        OutputExtension = ".txt",
        Options = new SortParam[]
        {
            new SortParam<string>(SortParam.DsaSplitType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEDSAParams.DSASplitTypes.Arbitrage}"),
            new SortParam<string>(SortParam.NationalSupplyChain.PropertyName, "4465782"),
            new SortParam<string>(SortParam.ZonalSupplyChain.PropertyName, "5364784"),
            new SortParam<string>(SortParam.MailmarkBatchRef.PropertyName, "1234"),
            new SortParam<string>(SortParam.Piece.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Pieces.Letter}"),
            new SortParam<string>(SortParam.ServiceLevel.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.ServiceLevels.Second}"),
            new SortParam<string>(SortParam.MailingRef.PropertyName, "#1027"),
            new SortParam<string>(SortParam.Container.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Containers.Bags}"),
            new SortParam<string>(SortParam.SortType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.SortTypes.Mailmark}"),
            new SortParam<string>(SortParam.MailType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.MailTypes.Business}"),
            new SortParam<int>(SortParam.Weight.PropertyName, 55),
            new SortParam<int>(SortParam.MaxContainerContentWeight.PropertyName, 9900),
            new SortParam<int>(SortParam.MinimumSelectionSize.PropertyName, 1),
            new SortParam<string>(SortParam.Sustain.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Sustainabilities.Standard}"),
            new SortParam<bool>(SortParam.Sort48.PropertyName, false),
            new SortParam<string>(SortParam.DirectResidueOrder.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.DirectResidueOrders.AlternateDirectResidue}"),
            new SortParam<bool>(SortParam.AllowSelectionTrayWithdrawal.PropertyName, false),
            new SortParam<bool>(SortParam.AllowAutoDpsLevelAdjustment.PropertyName, false),
            new SortParam<bool>(SortParam.SwitchOffVolumeChecking.PropertyName, false),
            new SortParam<bool>(SortParam.DsaMachinable.PropertyName, true),
        }
    };
```

#### Onepost Mailmark
```C#
    var sortDefinition = new SortDefinition
    {
        Description = "1722OnePostMM",
        Carrier = USEParams.Carriers.OnePost,
        Sorted = true,
        MailMark = true,
        InputFile = InputFile,
        OutputDirectory = testDirectory,
        OutputName = "Output",
        OutputExtension = ".txt",
        Options = new SortParam[]
        {
            new SortParam<string>(SortParam.DsaSplitType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEDSAParams.DSASplitTypes.Arbitrage}"),
            new SortParam<string>(SortParam.NationalSupplyChain.PropertyName, "4465782"),
            new SortParam<string>(SortParam.ZonalSupplyChain.PropertyName, "5364784"),
            new SortParam<string>(SortParam.MailmarkBatchRef.PropertyName, "1234"),
            new SortParam<string>(SortParam.Piece.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Pieces.Letter}"),
            new SortParam<string>(SortParam.ServiceLevel.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.ServiceLevels.Second}"),
            new SortParam<string>(SortParam.MailingRef.PropertyName, "#1722"),
            new SortParam<string>(SortParam.Container.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Containers.Bags}"),
            new SortParam<string>(SortParam.SortType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.SortTypes.Mailmark}"),
            new SortParam<string>(SortParam.MailType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.MailTypes.Business}"),
            new SortParam<int>(SortParam.Weight.PropertyName, 55),
            new SortParam<int>(SortParam.MaxContainerContentWeight.PropertyName, 10400),
            new SortParam<int>(SortParam.MinimumSelectionSize.PropertyName, 25),
            new SortParam<string>(SortParam.Sustain.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Sustainabilities.Standard}"),
            new SortParam<bool>(SortParam.Sort48.PropertyName, false),
            new SortParam<string>(SortParam.DirectResidueOrder.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.DirectResidueOrders.AlternateDirectResidue}"),
            new SortParam<bool>(SortParam.AllowSelectionTrayWithdrawal.PropertyName, false),
            new SortParam<bool>(SortParam.AllowAutoDpsLevelAdjustment.PropertyName, false),
            new SortParam<bool>(SortParam.SwitchOffVolumeChecking.PropertyName, false),
            new SortParam<bool>(SortParam.DsaMachinable.PropertyName, true),
        }
    };
```

#### UK Mail Mailmark
```C#
    var sortDefinition = new SortDefinition
    {
        Description = "3599UKMailMM",
        Carrier = USEParams.Carriers.UKMail,
        Sorted = true,
        MailMark = true,
        InputFile = InputFile,
        OutputDirectory = testDirectory,
        OutputName = "Output",
        OutputExtension = ".txt",
        Options = new SortParam[]
        {
            new SortParam<string>(SortParam.DsaSplitType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEDSAParams.DSASplitTypes.National}"),
            new SortParam<string>(SortParam.NationalSupplyChain.PropertyName, "4465782"),
            new SortParam<string>(SortParam.MailmarkBatchRef.PropertyName, "1234"),
            new SortParam<string>(SortParam.Piece.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Pieces.Letter}"),
            new SortParam<string>(SortParam.ServiceLevel.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.ServiceLevels.Second}"),
            new SortParam<string>(SortParam.MailingRef.PropertyName, "#3599"),
            new SortParam<string>(SortParam.Container.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Containers.Bags}"),
            new SortParam<string>(SortParam.SortType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.SortTypes.Mailmark}"),
            new SortParam<string>(SortParam.MailType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.MailTypes.Business}"),
            new SortParam<int>(SortParam.Weight.PropertyName, 55),
            new SortParam<int>(SortParam.MaxContainerContentWeight.PropertyName, 10800),
            new SortParam<int>(SortParam.MinimumSelectionSize.PropertyName, 25),
            new SortParam<string>(SortParam.Sustain.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Sustainabilities.Standard}"),
            new SortParam<bool>(SortParam.Sort48.PropertyName, false),
            new SortParam<string>(SortParam.DirectResidueOrder.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.DirectResidueOrders.AlternateDirectResidue}"),
            new SortParam<bool>(SortParam.AllowSelectionTrayWithdrawal.PropertyName, false),
            new SortParam<bool>(SortParam.AllowAutoDpsLevelAdjustment.PropertyName, false),
            new SortParam<bool>(SortParam.SwitchOffVolumeChecking.PropertyName, false),
            new SortParam<bool>(SortParam.DsaMachinable.PropertyName, true),
        }
    };
```

#### Whistl Mailmark
```C#
    var sortDefinition = new SortDefinition
    {
        Description = "4466WhistlMM",
        Carrier = USEParams.Carriers.Whistl,
        Sorted = true,
        MailMark = true,
        InputFile = InputFile,
        OutputDirectory = testDirectory,
        OutputName = "Output",
        OutputExtension = ".txt",
        Options = new SortParam[]
        {
            new SortParam<string>(SortParam.DsaSplitType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEDSAParams.DSASplitTypes.National}"),
            new SortParam<string>(SortParam.NationalSupplyChain.PropertyName, "4465782"),
            new SortParam<string>(SortParam.MailmarkBatchRef.PropertyName, "1234"),
            new SortParam<string>(SortParam.Piece.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Pieces.Letter}"),
            new SortParam<string>(SortParam.ServiceLevel.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.ServiceLevels.Second}"),
            new SortParam<string>(SortParam.MailingRef.PropertyName, "#4466"),
            new SortParam<string>(SortParam.Container.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Containers.Bags}"),
            new SortParam<string>(SortParam.SortType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.SortTypes.Mailmark}"),
            new SortParam<string>(SortParam.MailType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.MailTypes.Business}"),
            new SortParam<int>(SortParam.Weight.PropertyName, 55),
            new SortParam<int>(SortParam.MaxContainerContentWeight.PropertyName, 10400),
            new SortParam<int>(SortParam.MinimumSelectionSize.PropertyName, 25),
            new SortParam<string>(SortParam.Sustain.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Sustainabilities.Standard}"),
            new SortParam<bool>(SortParam.Sort48.PropertyName, false),
            new SortParam<string>(SortParam.DirectResidueOrder.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.DirectResidueOrders.AlternateDirectResidue}"),
            new SortParam<bool>(SortParam.AllowSelectionTrayWithdrawal.PropertyName, false),
            new SortParam<bool>(SortParam.AllowAutoDpsLevelAdjustment.PropertyName, false),
            new SortParam<bool>(SortParam.SwitchOffVolumeChecking.PropertyName, false),
            new SortParam<bool>(SortParam.DsaMachinable.PropertyName, true),
        }
    };
```

#### Royal Mail Mailmark
```C#
    var sortDefinition = new SortDefinition
    {
        Description = "61RoyalMailMM",
        Carrier = USEParams.Carriers.RoyalMail,
        Sorted = true,
        MailMark = true,
        InputFile = InputFile,
        OutputDirectory = testDirectory,
        OutputName = "Output",
        OutputExtension = ".txt",
        Options = new SortParam[]
        {
            new SortParam<string>(SortParam.NationalSupplyChain.PropertyName, "4465782"),
            new SortParam<string>(SortParam.MailmarkBatchRef.PropertyName, "1234"),
            new SortParam<string>(SortParam.Piece.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Pieces.Letter}"),
            new SortParam<string>(SortParam.ServiceLevel.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.ServiceLevels.Economy}"),
            new SortParam<string>(SortParam.MailingRef.PropertyName, "#61"),
            new SortParam<string>(SortParam.Container.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Containers.Bags}"),
            new SortParam<string>(SortParam.SortType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.SortTypes.Mailmark}"),
            new SortParam<string>(SortParam.MailType.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.MailTypes.Business}"),
            new SortParam<int>(SortParam.Weight.PropertyName, 55),
            new SortParam<int>(SortParam.MaxContainerContentWeight.PropertyName, 10500),
            new SortParam<int>(SortParam.MinimumSelectionSize.PropertyName, 99999),
            new SortParam<string>(SortParam.Sustain.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.Sustainabilities.Standard}"),
            new SortParam<bool>(SortParam.Sort48.PropertyName, false),
            new SortParam<string>(SortParam.DirectResidueOrder.PropertyName, $"{SoftwareBureau.UniversalSortEngine.USEParams.DirectResidueOrders.AlternateDirectResidue}"),
            new SortParam<bool>(SortParam.AllowSelectionTrayWithdrawal.PropertyName, false),
            new SortParam<bool>(SortParam.AllowAutoDpsLevelAdjustment.PropertyName, false),
            new SortParam<bool>(SortParam.SwitchOffVolumeChecking.PropertyName, false),
        }
    };
```
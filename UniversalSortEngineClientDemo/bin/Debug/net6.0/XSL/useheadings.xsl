<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <!-- BANNER PAGE VARIABLES -->
    <xsl:variable name="ProjectClientName" select="/Report/ApplicationData/Project/Client"/>
    <xsl:variable name="ProjectName" select="/Report/ApplicationData/Project/ProjectName"/>
    <xsl:variable name="ProjectId" select="/Report/ApplicationData/Project/ProjectId"/>
    <xsl:variable name="ProjectReference" select="/Report/ApplicationData/Project/ProjectReference"/>
    
    <xsl:variable name="ProcessDate" select="/Report/ApplicationData/Process/Date"/>
    <xsl:variable name="ProcessTime" select="/Report/ApplicationData/Process/Time"/>
    <xsl:variable name="ProcessApplicationVersion" select="/Report/ApplicationData/Process/Version"/>
    
    <xsl:variable name="UserName" select="/Report/ApplicationData/User/UserName"/>
    <xsl:variable name="UserCompany" select="/Report/ApplicationData/User/Company"/>
    <xsl:variable name="UserTelephone" select="/Report/ApplicationData/User/Telephone"/>
    <xsl:variable name="UserFax" select="/Report/ApplicationData/User/Fax"/>
    <xsl:variable name="UserWWW" select="/Report/ApplicationData/User/WWW"/>
    <xsl:variable name="Address" select="/Report/ApplicationData/User/Address"/>
            
    <xsl:template name="LayoutMasterSet">
        <fo:layout-master-set>
            <!-- A4 PAGE DEFINITIONS -->
            <!-- Banner Page different to standard page in having 2.5cm left and right margins  -->
            <fo:simple-page-master master-name="BannerPageA4Portrait" page-height="29.7cm"
                page-width="21cm" margin-left="2.5cm" margin-right="2.5cm">
                <fo:region-before extent="4cm"/>
                <fo:region-body margin-top="4cm" margin-bottom="2cm"/>
                <fo:region-after extent="2cm"/>
            </fo:simple-page-master>
            <!-- Standard (report detail) Page different to Banner Page in having 1cm left and right margins  -->
            <fo:simple-page-master master-name="A4Portrait" page-height="29.7cm"
                page-width="21cm" margin-left="1cm" margin-right="1cm">
                <fo:region-before extent="3cm"/>
                <fo:region-body margin-top="3cm" margin-bottom="15mm"/>
                <fo:region-after extent="15mm"/>
            </fo:simple-page-master>
            <!-- Banner Page different to standard page in having 2.5cm left and right margins  -->
            <fo:simple-page-master master-name="BannerPageA4Landscape" page-height="21cm"
                page-width="29.7cm" margin-left="2.5cm" margin-right="2.5cm">
                <fo:region-before extent="4cm"/>
                <fo:region-body margin-top="4cm" margin-bottom="1.8cm"/>
                <fo:region-after extent="1.8cm"/>
            </fo:simple-page-master>
            <!-- Standard (report detail) Page different to Banner Page in having 1cm left and right margins  -->
            <fo:simple-page-master master-name="A4Landscape" page-height="21cm"
                page-width="29.7cm" margin-left="1cm" margin-right="1cm">
                <fo:region-before extent="3cm"/>
                <fo:region-body margin-top="3cm" margin-bottom="2cm"/>
                <fo:region-after extent="2cm"/>
            </fo:simple-page-master>
            
            <!-- A3 PAGE DEFINITIONS -->
            <!-- Banner Page different to standard page in having 2.5cm left and right margins  -->
            <fo:simple-page-master master-name="BannerPageA3Portrait" page-height="42cm"
                page-width="29.7cm" margin-left="2.5cm" margin-right="2.5cm">
                <fo:region-before extent="4cm"/>
                <fo:region-body margin-top="4cm" margin-bottom="2cm"/>
                <fo:region-after extent="2cm"/>
            </fo:simple-page-master>
            <!-- Standard (report detail) Page different to Banner Page in having 1cm left and right margins  -->
            <fo:simple-page-master master-name="A3Portrait" page-height="42cm"
                page-width="29.7cm" margin-left="1cm" margin-right="1cm">
                <fo:region-before extent="3cm"/>
                <fo:region-body margin-top="3cm" margin-bottom="15mm"/>
                <fo:region-after extent="15mm"/>
            </fo:simple-page-master>
            <!-- Banner Page different to standard page in having 2.5cm left and right margins  -->
            <fo:simple-page-master master-name="BannerPageA3Landscape" page-height="29.7cm"
                page-width="42cm" margin-left="2.5cm" margin-right="2.5cm">
                <fo:region-before extent="4cm"/>
                <fo:region-body margin-top="4cm" margin-bottom="1.8cm"/>
                <fo:region-after extent="1.8cm"/>
            </fo:simple-page-master>
            <!-- Standard (report detail) Page different to Banner Page in having 1cm left and right margins  -->
            <fo:simple-page-master master-name="A3Landscape" page-height="29.7cm"
                page-width="42cm" margin-left="1cm" margin-right="1cm">
                <fo:region-before extent="3cm"/>
                <fo:region-body margin-top="3cm" margin-bottom="2cm"/>
                <fo:region-after extent="2cm"/>
            </fo:simple-page-master>
        </fo:layout-master-set>        
    </xsl:template>
    <xsl:template name="Logo">
        <xsl:param name="GraphicFile"></xsl:param>
        <fo:external-graphic>
            <xsl:attribute name="src"><xsl:value-of select="$GraphicFile"/></xsl:attribute>
        </fo:external-graphic>
    </xsl:template>
    <!-- Footer output at the bottom of each page of all reports - works for all size and orientations of paper -->
    <xsl:template name="Footer">
        <fo:static-content flow-name="xsl-region-after">
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-width="6cm"/>
                <fo:table-column column-width="proportional-column-width(1)"/>	<!-- to center table across page -->
                <fo:table-column column-width="6cm"/>
                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell>
                            <fo:block space-before="2mm" text-align="left" font-family="Helvetica"
                                font-size="6pt"> Copyright The Software Bureau Limited 2012-<xsl:value-of select="substring($ProcessDate,7,4)"/></fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block space-before="2mm" text-align="center" font-family="Helvetica"
                                font-size="8pt"> Page <fo:page-number/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block space-before="2mm" text-align="right" font-family="Helvetica"
                                font-size="6pt">
                                <xsl:value-of select="$UserWWW"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                        <fo:table-cell>
                            <fo:block space-before="2mm" text-align="left" font-family="Helvetica"
                                font-size="6pt">
                                <fo:inline>
                                    <xsl:value-of select="$UserCompany"/>
                                </fo:inline>
                                <fo:inline> Tel: <xsl:value-of select="$UserTelephone"/>
                                </fo:inline>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell>  
                            <fo:block space-before="2mm" text-align="center" font-family="Helvetica"
                                font-size="6pt">
                                <xsl:for-each select="/Report//ApplicationData/User/Address/Line">
                                    <xsl:choose>
                                        <xsl:when test="(position()=1)">
                                            <xsl:value-of select="."/>
                                        </xsl:when>
                                        <xsl:otherwise> , <xsl:value-of select="."/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:for-each>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block space-before="2mm" text-align="right" font-family="Helvetica"
                                font-size="6pt"><xsl:value-of select="$ProcessDate"/>&#160;<xsl:value-of select="$ProcessTime"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                        <fo:table-cell>
                            <fo:block space-before="2mm" text-align="left" font-family="Helvetica"
                                font-size="6pt">Swift Sort <xsl:value-of select="$ProcessApplicationVersion"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:static-content>
    </xsl:template>
    
    <!-- Decides upon PaddingTop then  passes to HeaderActual to print the header -->
    <xsl:template name="Header">
        <xsl:param name="Title"/>
        <xsl:param name="USE-APIDemo"/>       
        <!-- Fonet appears to have a border at the top of the page whereas Ibex permits printing at the top edge of the page  -->
        <xsl:choose>
            <xsl:when test="$USE-APIDemo = 'true'">
                <xsl:call-template name="HeaderActual">
                    <xsl:with-param name="Title" select="$Title"/>
                    <xsl:with-param name="PaddingTop" select="'0mm'"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="HeaderActual">
                    <xsl:with-param name="Title" select="$Title"/>
                    <xsl:with-param name="PaddingTop" select="'10mm'"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
        
    <!-- Header template that actually prints the header
         Header output at the top of each page of all reports - works for all size and orientations of paper -->
    <xsl:template name="HeaderActual">
        <xsl:param name="Title"/>
        <xsl:param name="PaddingTop"/>
        <fo:static-content flow-name="xsl-region-before">
        <fo:table table-layout="fixed" width="100%">
            <fo:table-column column-width="2cm"/>
            <fo:table-column column-width="proportional-column-width(1)"/>	 <!-- to center table across page -->
            <fo:table-column column-width="2cm"/>
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell>
                        <fo:block/>
                    </fo:table-cell>
                    <fo:table-cell>
                        <fo:block padding-top="{$PaddingTop}" space-before="12mm" space-after="5mm" width="17cm" height="5cm" 
                                font-family="Helvetica" font-size="18pt"
                                font-weight="bold" text-align="center">
                                <xsl:value-of select="$Title"/>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell>
                        <fo:block/>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
        </fo:static-content>
    </xsl:template>
    
    <!-- Text output on the first Banner Page of each report -->
    <xsl:template name="BannerPageProjectDetails">
        <fo:block space-before="1.5cm" font-family="Helvetica" font-weight="bold" font-size="18pt">
            Project Details </fo:block>
        <fo:block space-before="3mm" font-family="Helvetica" font-size="16pt">
            <fo:inline font-weight="bold"> Client: </fo:inline>
            <fo:inline>
                <xsl:value-of select="$ProjectClientName"/>
            </fo:inline>
        </fo:block>
        <fo:block space-before="3mm" font-family="Helvetica" font-size="16pt">
            <fo:inline font-weight="bold"> Job: </fo:inline>
            <fo:inline>
                <xsl:value-of select="$ProjectName"/> (<xsl:value-of select="$ProjectId"/>) </fo:inline>
        </fo:block>
        
        <fo:block space-before="3mm" font-family="Helvetica" font-size="16pt">
            <fo:inline font-weight="bold"> Mail Cell Ref: </fo:inline>
            <fo:inline>
                <xsl:value-of select="$ProjectReference"/>
            </fo:inline>
        </fo:block>  
   </xsl:template>
    <xsl:template name="BannerPageProcessDetails">
        <fo:block space-before="1.5cm" font-family="Helvetica" font-weight="bold" font-size="18pt">
            Process Details </fo:block>
        <fo:block space-before="3mm" font-family="Helvetica" font-size="16pt">
            <fo:inline font-weight="bold">Completed: </fo:inline>
            <fo:inline> on <xsl:value-of select="$ProcessDate"/> at <xsl:value-of
                select="$ProcessTime"/>
            </fo:inline>
        </fo:block>   
        <fo:block space-before="3mm" font-family="Helvetica" font-size="16pt">
            <fo:inline font-weight="bold">Using Version: </fo:inline>
            <fo:inline><xsl:value-of select="$ProcessApplicationVersion"/></fo:inline>
        </fo:block>        
    </xsl:template>
    <xsl:template name="BannerPagePreparedBy">
        <fo:block space-before="1.5cm" font-family="Helvetica" font-weight="bold" font-size="18pt">
            Report Prepared By</fo:block>
        <fo:block space-before="3mm" font-family="Helvetica" font-size="16pt">
            <fo:inline>
                <xsl:value-of select="$UserName"/>
            </fo:inline>
        </fo:block>
        <fo:block space-before="3mm" font-family="Helvetica" font-size="16pt">
            <fo:inline>
                <xsl:value-of select="$UserCompany"/> <!-- - tel <xsl:value-of select="contact/tel"/> -->
            </fo:inline>
        </fo:block>
        <fo:block space-before="3mm" font-family="Helvetica" font-size="16pt">
            <fo:inline>
                Tel: <xsl:value-of select="$UserTelephone"/>
            </fo:inline>
        </fo:block>
	<xsl:if test="contact/fax != ''">
        <fo:block space-before="3mm" font-family="Helvetica" font-size="16pt">
            <fo:inline>
                Fax: <xsl:value-of select="$UserFax"/>
            </fo:inline>
        </fo:block>
	</xsl:if>
        <fo:block space-before="3mm" font-family="Helvetica" font-size="16pt">
            <xsl:for-each select="/Report//ApplicationData/User/Address/Line">
                <xsl:choose>
                    <xsl:when test="(position()=1)">
                        <xsl:value-of select="."/>
                    </xsl:when>
                    <xsl:otherwise>, <xsl:value-of select="."/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </fo:block>
        <fo:block space-before="3mm" font-family="Helvetica" font-size="16pt">
            <xsl:value-of select="$UserWWW"/>
        </fo:block>        
    </xsl:template>
    <xsl:template name="AllBannerPages">      
        <xsl:param name="Title"/>
        <fo:block space-before="1.8cm" font-family="Helvetica" font-size="20pt" font-weight="bold"
            text-align="center"><xsl:value-of select="$Title"/></fo:block>
        <fo:block space-before="5mm" font-family="Helvetica" font-size="20pt" font-weight="bold"
            text-align="center">
            <xsl:value-of select="@name"/>
        </fo:block>
        <xsl:call-template name="BannerPageProjectDetails"></xsl:call-template>
        <xsl:call-template name="BannerPageProcessDetails"></xsl:call-template>
        <xsl:call-template name="BannerPagePreparedBy"></xsl:call-template>
    </xsl:template>
</xsl:stylesheet>

﻿<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output omit-xml-declaration="yes" indent="no"/>
	<xsl:template match="/Report">
		<xsl:call-template name="UnsortedListing">
			<xsl:with-param name="nodes" select="BagBreakdown/NationalSelections" />				
		</xsl:call-template>
		<xsl:call-template name="UnsortedListing">
			<xsl:with-param name="nodes" select="BagBreakdown/BFPOSelections" />				
		</xsl:call-template>
		<xsl:call-template name="UnsortedListing">
			<xsl:with-param name="nodes" select="BagBreakdown/InternationalSelections" />				
		</xsl:call-template>
    </xsl:template>	

	<xsl:template name="UnsortedListing">
		<xsl:param name="nodes"/>
        	<xsl:for-each select="$nodes/Selection"><xsl:value-of select="Number"/>,<xsl:value-of select="Id"/>,<xsl:value-of select="Start"/>,<xsl:value-of select="End"/>,<xsl:value-of select="Items"/><xsl:text>&#10;</xsl:text></xsl:for-each>        
	</xsl:template>	
</xsl:stylesheet>
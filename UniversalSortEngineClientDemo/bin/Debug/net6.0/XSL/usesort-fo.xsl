﻿<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:include href="fonts.xsl"/><!-- Fonts used by all paper reports  -->
	<xsl:include href="usevariables.xsl"/>								<!-- Variables -->
	<xsl:include href="useheadings.xsl"/>								<!-- LayoutMasterSet, Header Page, Headers, Footers  -->
	<xsl:variable name="A4-printable-width" select="190"/>				<!-- A4-printable-width is 210mm less 10mm left and right margin -->
	<xsl:variable name="A4-Landscape-Printable-Width" select="275"/>	<!-- The output format declaration -->
	<xsl:variable name="BorderStyle" select="'none'"/>
	<!-- XSL originally written for Ibex which supports proportional column widths in tables.
		The API Demo uses FONet which does not support proportional column widths. 
		$USE-APIDemo also controls the page top margin and the Graphic used at the top of the first page. -->
	<xsl:variable name="USE-APIDemo" select="'true'"/>					<!-- true for the USE API Demo, false for USE SwiftSort -->	
	<xsl:variable name="Graphic">										<!-- Different Graphic for USE API Demo to SwiftSort -->
		<xsl:choose>
			<xsl:when test="$USE-APIDemo = 'true'"><xsl:value-of select="'USE.png'"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="'ReportLogo.jpg'"/></xsl:otherwise>			
		</xsl:choose>
	</xsl:variable>	
	<xsl:variable name="HeaderText">									<!-- Different Header for USE API Demo to SwiftSort -->
		<xsl:choose>
			<xsl:when test="$USE-APIDemo = 'true'"><xsl:value-of select="'USE API DEMO'"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="'SWIFT SORT'"/></xsl:otherwise>
		</xsl:choose>		
	</xsl:variable>
		
	<xsl:output method="xml"/>
	<xsl:template match="/Report">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
			<xsl:call-template name="LayoutMasterSet"/>					<!-- LayoutMasterSet contains all layout-master-sets for all page definitions used by our reports -->	
			<xsl:choose>
				<xsl:when test="$UnSorted">
					
					<fo:page-sequence master-reference="BannerPageA4Portrait">
						<xsl:call-template name="BannerPageContentsA4Portrait"/>
					</fo:page-sequence>										
					
					<fo:page-sequence master-reference="A4Portrait">
						<xsl:call-template name="UnsortedPlanningPageController"/>								
					</fo:page-sequence>
					
					<fo:page-sequence master-reference="A4Portrait">
						<xsl:call-template name="UnSortedListingController"/>
					</fo:page-sequence>
				</xsl:when>
				<xsl:when test="$mixedweight">
					<fo:page-sequence master-reference="BannerPageA4Landscape">
						<xsl:call-template name="BannerPageContentsA4Landscape"/>
					</fo:page-sequence>					

					<fo:page-sequence master-reference="A4Landscape">
						<xsl:call-template name="PlanningPageController"/>
					</fo:page-sequence>
					
					<fo:page-sequence master-reference="A4Landscape">
						<xsl:call-template name="Header">
							<xsl:with-param name="Title" select="'Listing Criteria'"/>
							<xsl:with-param name="USE-APIDemo" select="$USE-APIDemo"/>
						</xsl:call-template>
						<xsl:call-template name="Footer"/>
						<fo:flow flow-name="xsl-region-body" font-family="{$d1-family}" font-size="{$d1-size}" font-weight="normal">
							<xsl:call-template name="ListingHeaderPage"/>
						</fo:flow>
					</fo:page-sequence>
										
					<!-- The Output Files page -->
					<xsl:if test="$onepost or $ukmail or $tnt">					
						<fo:page-sequence master-reference="A4Landscape">
							<xsl:call-template name="Header">
								<xsl:with-param name="Title" select="'Output Files'"/>
								<xsl:with-param name="USE-APIDemo" select="$USE-APIDemo"/>
							</xsl:call-template>
							<xsl:call-template name="Footer"/>
							<fo:flow flow-name="xsl-region-body" font-family="{$d1-family}" font-size="{$d1-size}" font-weight="normal">
								<xsl:call-template name="PlanningOutputFiles"/>
							</fo:flow>
						</fo:page-sequence>
					</xsl:if>
					
					<!-- The Listing pages for Mixed Weight Sorts -->										
					<xsl:choose>
						<xsl:when test="$lowsort">
							<xsl:call-template name="ListingMixedWeightLowSort"/>	
						</xsl:when>
						<xsl:when test="$DirectResidueAlternateOrder = 'SeparateResidueDirect'">
							<xsl:call-template name="ListingMixedWeightResidueDirect"/>	
						</xsl:when>
						<xsl:when test="$DirectResidueAlternateOrder = 'SeparateDirectResidue'">
							<xsl:call-template name="ListingMixedWeightDirectResidue"/>
						</xsl:when>
						<xsl:otherwise>
							<!-- We can deal with the two interleaved with the same template because the difference is dictated by the order of the data -->
							<xsl:call-template name="ListingMixedWeightAlternate"/>	
						</xsl:otherwise>
					</xsl:choose>					
				</xsl:when>
				<xsl:otherwise>
					<fo:page-sequence master-reference="BannerPageA4Portrait">
						<xsl:call-template name="BannerPageContentsA4Portrait"/>
					</fo:page-sequence>										

					<fo:page-sequence master-reference="A4Portrait">
						<xsl:call-template name="PlanningPageController"/>
					</fo:page-sequence>

					<fo:page-sequence master-reference="A4Portrait">
						<xsl:call-template name="Header">
							<xsl:with-param name="Title" select="'Listing Criteria'"/>
							<xsl:with-param name="USE-APIDemo" select="$USE-APIDemo"/>							
						</xsl:call-template>
						<xsl:call-template name="Footer"/>
						<fo:flow flow-name="xsl-region-body" font-family="{$d1-family}" font-size="{$d1-size}" font-weight="normal">
							<xsl:call-template name="ListingHeaderPage"/>
						</fo:flow>
					</fo:page-sequence>	
										
					<xsl:if test="$onepost or $ukmail or $tnt">						
						<!-- The Output Files page -->
						<fo:page-sequence master-reference="A4Portrait">
							<xsl:call-template name="Header">
								<xsl:with-param name="Title" select="'Output Files'"/>
								<xsl:with-param name="USE-APIDemo" select="$USE-APIDemo"/>
							</xsl:call-template>
							<xsl:call-template name="Footer"/>
							<fo:flow flow-name="xsl-region-body" font-family="{$d1-family}" font-size="{$d1-size}" font-weight="normal">
								<xsl:call-template name="PlanningOutputFiles"/>
							</fo:flow>
						</fo:page-sequence>							
					</xsl:if>
					
					<!-- The Listing pages for Fixed Weight Sorts -->					
					<xsl:choose>
						<xsl:when test="$lowsort">
							<xsl:call-template name="ListingLowSort"/>	
						</xsl:when>
						<xsl:when test="$DirectResidueAlternateOrder = 'SeparateResidueDirect'">
							<xsl:call-template name="ListingResidueDirect"/>	
						</xsl:when>
						<xsl:when test="$DirectResidueAlternateOrder = 'SeparateDirectResidue'">
							<xsl:call-template name="ListingDirectResidue"/>
						</xsl:when>
						<xsl:otherwise>
							<!-- We can deal with the two interleaved with the same template because the difference is dictated by the order of the data -->
							<xsl:call-template name="ListingAlternate"/>	
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
			
			<!-- The Standard Tariff pages - only if not UnSorted -->
			<xsl:if test="not($UnSorted)">
				<fo:page-sequence master-reference="A4Portrait">
					<xsl:call-template name="Header">					
						<xsl:with-param name="Title" select="'Standard Tariff Items'"/>
						<xsl:with-param name="USE-APIDemo" select="$USE-APIDemo"/>
					</xsl:call-template>
					<xsl:call-template name="Footer"/>
					<fo:flow flow-name="xsl-region-body" font-family="{$d1-family}" font-size="{$d1-size}" font-weight="normal">
						<xsl:call-template name="StandardRows"/>
					</fo:flow>
				</fo:page-sequence>
			</xsl:if>
		</fo:root>
	</xsl:template>
	
	<xsl:template name="BlankLine"><fo:block>&#xA0;</fo:block></xsl:template>			<!-- Includes a blank line -->
	
	<xsl:template name="PlanningPageController">
		<xsl:call-template name="Header">
			<xsl:with-param name="Title" select= "$PlanningAnalysis"/> 
			<xsl:with-param name="USE-APIDemo" select="$USE-APIDemo"/>			
		</xsl:call-template>
		<xsl:call-template name="Footer"/>
		<fo:flow flow-name="xsl-region-body" font-family="{$d1-family}" font-size="{$d1-size}" font-weight="normal">
			<xsl:if test="not($SortOK)">
				<xsl:call-template name="PlanningInvalid">
         				<xsl:with-param name="ErrorMessage" select="$ErrorMessage"/>
       			</xsl:call-template>
			</xsl:if>
			<xsl:call-template name="PlanningParameters"/>
			
			<xsl:choose>
				<xsl:when test="$mixedweight">
					<!-- For Mixed Weight we output Presortation and Weight Analsysis across page at same vertical position of report and then Postcode Statistics under Presortation Statistics-->
					<!-- So first we create a table to split the page into three columns -->
					<fo:table space-before="5mm" table-layout="fixed">
						<fo:table-column column-width="80mm"/>
						<fo:table-column column-width="14mm"/>
						<fo:table-column column-width="66mm"/>
						<fo:table-column column-width="20mm"/>
						<fo:table-column column-width="90mm"/>
						<fo:table-column column-width="10mm"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell>														<!-- LHS of Page i.e. Presortation Statistics -->
									<xsl:call-template name="PlanningPresortationStatistics"/>
								</fo:table-cell>
								<fo:table-cell/>													<!-- For spacing -->
								<fo:table-cell/>													<!-- For spacing -->
								<fo:table-cell/>													<!-- For spacing -->
								<fo:table-cell>														<!-- RHS of Page i.e. Weight Analysis -->
									<xsl:call-template name="PlanningWeights"/>
								</fo:table-cell>
								<fo:table-cell/>													<!-- For spacing -->
							</fo:table-row>
							
							<fo:table-row>
								<fo:table-cell>
									<xsl:call-template name="PlanningPostcodeStatistics">
										<xsl:with-param name="mixedWeight" select="'true'"/>											
									</xsl:call-template>
								</fo:table-cell>
								<fo:table-cell/>													<!-- For spacing -->								
								<fo:table-cell>
									<xsl:if test="$mailmark">
										<xsl:call-template name="PlanningDPSStats">
											<xsl:with-param name="mixedWeight" select="'true'"/>
										</xsl:call-template>
									</xsl:if>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell><fo:block></fo:block>
									<xsl:call-template name="PlanningSortStats"></xsl:call-template>
								</fo:table-cell>
							</fo:table-row>
							<xsl:if test="not($dsa)">
								<fo:table-row>
									<fo:table-cell><fo:block></fo:block>
										<xsl:call-template name="PlanningCosts"></xsl:call-template>
									</fo:table-cell>
								</fo:table-row>								
							</xsl:if>
						</fo:table-body>
					</fo:table>
				</xsl:when>
				<xsl:otherwise>
					<!-- For Fixed Weight we output Presortation, Postcode and (optionally) DPS Statistics across page at same vertical position of report -->
					<!-- So first we create a table to split the page into three columns -->
					<fo:table space-before="5mm" table-layout="fixed">
						<fo:table-column column-width="80mm"/>
						<fo:table-column column-width="10mm"/>
						<fo:table-column column-width="50mm"/>
						<fo:table-column column-width="10mm"/>
						<fo:table-column column-width="40mm"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell>														<!-- LHS of Page i.e. Presortation Statistics -->
									<xsl:call-template name="PlanningPresortationStatistics"/>
								</fo:table-cell>
								<fo:table-cell/>													<!-- For spacing -->
								<fo:table-cell>														<!-- RHS of Page i.e. Postcode Statistics -->
									<xsl:call-template name="PlanningPostcodeStatistics">
										<xsl:with-param name="mixedWeight" select="'false'"/>
									</xsl:call-template>
								</fo:table-cell>
								<fo:table-cell/>													<!-- For spacing -->
								<fo:table-cell>
									<xsl:if test="contains($mailsortservice,'Barcode') or contains($mailsortservice,'CBC') or contains($mailsortservice,'Mailmark')   ">
										<xsl:call-template name="PlanningDPSStats">
											<xsl:with-param name="mixedWeight" select="'false'"/>
										</xsl:call-template>
									</xsl:if>														
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
					
					<xsl:call-template name="PlanningSortStats"/>
					
					<xsl:if test="not($dsa)">
						<xsl:call-template name="PlanningCosts"/>
					</xsl:if>				
				</xsl:otherwise>
			</xsl:choose>
			
			<xsl:if test="$tnt">
				<xsl:call-template name="PlanningWhistl"/>				
			</xsl:if>
			
			<xsl:if test="not($dsa) and not($mixedweight)">
				<xsl:call-template name="PlanningSavings"/>
				<xsl:call-template name="PlanningStandardTariff"/>
			</xsl:if>
		</fo:flow>		
	</xsl:template>	
	
	<!-- ***************************************         Banner Page       ***********************************-->	
	
	<xsl:template name="BannerPageContentsA4Portrait">
		<fo:static-content flow-name="xsl-region-before">
			<fo:table table-layout="fixed">
				<fo:table-column column-width="4cm"/>
				<fo:table-column column-width="8cm"/>
				<fo:table-column column-width="4cm"/>
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell>
							<fo:block/>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block space-before="8mm">
								<xsl:call-template name="Logo"><xsl:with-param name="GraphicFile" select="$Graphic"></xsl:with-param></xsl:call-template>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block space-before="8mm"/>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:static-content>
		<fo:flow flow-name="xsl-region-body" font-family="Helvetica" font-size="20pt">
			<xsl:call-template name="BlankLine"></xsl:call-template>
			<fo:block space-before="0.8cm" font-family="Helvetica" font-size="20pt" font-weight="bold" text-align="center"><xsl:value-of select="$HeaderText"/></fo:block>
			
			<fo:block space-before="5mm" font-family="Helvetica" font-size="20pt" font-weight="bold" text-align="center">
				<xsl:value-of select="@name"/>
			</fo:block>
			
			<xsl:call-template name="BannerPageProjectDetails"/>
			<xsl:call-template name="BannerPageProcessDetails"/>
			<xsl:call-template name="BannerPagePreparedBy"/>
		</fo:flow>
	</xsl:template>

	<xsl:template name="BannerPageContentsA4Landscape">
		<fo:static-content flow-name="xsl-region-before">
			<xsl:call-template name="BlankLine"></xsl:call-template>
			<fo:table table-layout="fixed">
				<fo:table-column column-width="20.7cm"/>
				<fo:table-column column-width="4cm"/>
				<fo:table-body>
					<fo:table-row>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell>
							<fo:block text-indent="100mm" font-family="Helvetica" font-size="20pt" font-weight="bold" text-align="left"> SWIFT SORT </fo:block>							
						</fo:table-cell>
						<fo:table-cell>
							<fo:block space-before="8mm">
								<xsl:call-template name="Logo"><xsl:with-param name="GraphicFile" select="$Graphic"></xsl:with-param></xsl:call-template>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:static-content>
		<fo:flow flow-name="xsl-region-body" font-family="Helvetica" font-size="20pt">
			<xsl:call-template name="BannerPageProjectDetails"/>
			<xsl:call-template name="BannerPageProcessDetails"/>
			<xsl:call-template name="BannerPagePreparedBy"/>
		</fo:flow>
	</xsl:template>
	
	<!-- ***************************************          Planning Analysis          ***********************************-->		
	<xsl:template name="PlanningInvalid">
		<fo:table table-layout="fixed" width="100%">
			<xsl:choose>
				<xsl:when test="$USE-APIDemo='true'">
					<fo:table-column column-width="4.5cm"/>
					<fo:table-column column-width="12cm"/>
					<fo:table-column column-width="4.5cm"/>										
				</xsl:when>
				<xsl:otherwise>
					<fo:table-column column-width="proportional-column-width(1)"/>
					<fo:table-column column-width="12cm"/>
					<fo:table-column column-width="proportional-column-width(1)"/>					
				</xsl:otherwise>				
			</xsl:choose>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
						<fo:block/>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="1mm" space-after="3mm" text-align="center" font-family="{$h3-family}" font-size="{$h3-size}" font-weight="bold" color="red"> <xsl:value-of select="$ErrorMessage"/></fo:block>
     		
					</fo:table-cell>
					<fo:table-cell>
						<fo:block/>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		</xsl:template>
	
	<xsl:template name="PlanningParametersRow">
		<xsl:param name="Title"/>
		<xsl:param name="Value"/>
		<fo:table-row>
			<fo:table-cell padding-left="2mm">
				<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal"><xsl:value-of select="$Title"/></fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-before="1mm" font-weight="bold">
					<xsl:value-of select="$Value"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>		
	</xsl:template>
	
	<xsl:template name="QuantityRow">
		<xsl:param name="Title"/>
		<xsl:param name="Value"/>
		<fo:table-row>
			<fo:table-cell padding-left="2mm">
				<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal"><xsl:value-of select="$Title"/></fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-before="1mm" font-weight="normal" text-align="right">
					<xsl:value-of select="format-number($Value,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>		
	</xsl:template>
	
	<xsl:template name="QuantityRowBold">
		<xsl:param name="Title"/>
		<xsl:param name="Value"/>
		<fo:table-row>
			<fo:table-cell padding-left="2mm">
				<fo:block space-before="2mm" space-after="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold"><xsl:value-of select="$Title"/></fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-before="2mm" space-after="1mm" font-weight="bold" text-align="right">
					<xsl:value-of select="format-number($Value,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>		
	</xsl:template>
	
	<xsl:template name="ListingParametersMailmark">		
		<fo:block space-before="5mm" font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Mailmark</fo:block>
		
		<fo:table table-layout="fixed">
			<fo:table-column column-width="90mm"/>
			<fo:table-column column-width="100mm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell><!-- LHS of Page i.e. Supply Chain -->
						<fo:table table-layout="fixed" width="100%">
							<xsl:choose>
								<xsl:when test="$USE-APIDemo='true'">
									<fo:table-column column-width="3.5cm"/>
									<fo:table-column column-width="5.5cm"/>									
								</xsl:when>
								<xsl:otherwise>
									<fo:table-column column-width="proportional-column-width(3)"/>
									<fo:table-column column-width="proportional-column-width(5)"/>
								</xsl:otherwise>
							</xsl:choose>
							<fo:table-body>
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Supply Chain ID'"/>
									<xsl:with-param name="Value" select="$MailmarkSupplyChainId"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Owner'"/>
									<xsl:with-param name="Value" select="$MailmarkOwnerName"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Payer'"/>
									<xsl:with-param name="Value" select="$MailmarkPayerName"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Producer'"/>
									<xsl:with-param name="Value" select="$MailmarkProducerName"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Return Postcode'"/>
									<xsl:with-param name="Value" select="$MailmarkReturnPostCode"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Campaign'"/>
									<xsl:with-param name="Value" select="$MailmarkCampaignName"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Department'"/>
									<xsl:with-param name="Value" select="$MailmarkDepartmentName"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Originator'"/>
									<xsl:with-param name="Value" select="$MailmarkOriginatorName"/>
								</xsl:call-template>				
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
					<fo:table-cell><!-- RHS of Page i.e. JIC Opt-Out -->
						<fo:table table-layout="fixed" width="100%">
							<xsl:choose>
								<xsl:when test="$USE-APIDemo='true'">
									<fo:table-column column-width="3.5cm"/>
									<fo:table-column column-width="5.5cm"/>									
								</xsl:when>
								<xsl:otherwise>
									<fo:table-column column-width="proportional-column-width(3)"/>
									<fo:table-column column-width="proportional-column-width(5)"/>
								</xsl:otherwise>
							</xsl:choose>
							<fo:table-body>
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'JIC Opt-out'"/>
									<xsl:with-param name="Value" select="$MailmarkSpare1-JICOptOut"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Mail Info Type ID'"/>
									<xsl:with-param name="Value" select="$MailmarkSpare2-MailInfoTypeID"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Spare-3'"/>
									<xsl:with-param name="Value" select="$MailmarkSpare3"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Spare-4'"/>
									<xsl:with-param name="Value" select="$MailmarkSpare4"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Spare-5'"/>
									<xsl:with-param name="Value" select="$MailmarkSpare5"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Spare-6'"/>
									<xsl:with-param name="Value" select="$MailmarkSpare6"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Spare-7'"/>
									<xsl:with-param name="Value" select="$MailmarkSpare7"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Spare-8'"/>
									<xsl:with-param name="Value" select="$MailmarkSpare8"/>
								</xsl:call-template>		
								<xsl:if test="$CustomerRef">
									<xsl:call-template name="PlanningParametersRow">
										<xsl:with-param name="Title" select="'Customer-Ref'"/>
										<xsl:with-param name="Value" select="$CustomerRef"/>
									</xsl:call-template>		
								</xsl:if>	
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Spare-10'"/>
									<xsl:with-param name="Value" select="$MailmarkSpare10"/>
								</xsl:call-template>				
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>

		<fo:table table-layout="fixed">
			<fo:table-column column-width="90mm"/>
			<fo:table-column column-width="100mm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell><!-- LHS of Page i.e. Supply Chain -->
						<fo:table table-layout="fixed" width="100%">
							<xsl:choose>
								<xsl:when test="$USE-APIDemo='true'">
									<fo:table-column column-width="3.5cm"/>
									<fo:table-column column-width="5.5cm"/>									
								</xsl:when>
								<xsl:otherwise>
									<fo:table-column column-width="proportional-column-width(3)"/>
									<fo:table-column column-width="proportional-column-width(5)"/>
								</xsl:otherwise>
							</xsl:choose>
							<fo:table-body>
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Mail-Sub-Type-1'"/>
									<xsl:with-param name="Value" select="$MailmarkMailSubType1"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Mail-Sub-Type-2'"/>
									<xsl:with-param name="Value" select="$MailmarkMailSubType2"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Mail-Sub-Type-3'"/>
									<xsl:with-param name="Value" select="$MailmarkMailSubType3"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Media Agency'"/>
									<xsl:with-param name="Value" select="$MailmarkMailSubType4"/>
								</xsl:call-template>				
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
					<fo:table-cell><!-- RHS of Page i.e. JIC Opt-Out -->
						<fo:table table-layout="fixed" width="100%">
							<xsl:choose>
								<xsl:when test="$USE-APIDemo='true'">
									<fo:table-column column-width="3.5cm"/>
									<fo:table-column column-width="5.5cm"/>									
								</xsl:when>
								<xsl:otherwise>
									<fo:table-column column-width="proportional-column-width(3)"/>
									<fo:table-column column-width="proportional-column-width(5)"/>
								</xsl:otherwise>
							</xsl:choose>
							<fo:table-body>
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Adv-Attribute-1'"/>
									<xsl:with-param name="Value" select="$MailmarkAdvertisingAttribute1"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Adv-Attribute-2'"/>
									<xsl:with-param name="Value" select="$MailmarkAdvertisingAttribute2"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Adv-Attribute-3'"/>
									<xsl:with-param name="Value" select="$MailmarkAdvertisingAttribute3"/>
								</xsl:call-template>				
								<xsl:call-template name="PlanningParametersRow">
									<xsl:with-param name="Title" select="'Incentive/Promotion'"/>
									<xsl:with-param name="Value" select="$MailmarkAdvertisingAttribute4"/>
								</xsl:call-template>				
							</fo:table-body>
						</fo:table>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>				
	</xsl:template>
				
	<xsl:template name="PlanningParameters">
		<xsl:choose>
			<xsl:when test="$TableEffectiveDateDay">
				<fo:block space-after="1mm" font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Mailsort Parameters (<xsl:value-of select="$TableEffectiveDateDay"/>/<xsl:value-of select="$TableEffectiveDateMonth"/>/<xsl:value-of select="$TableEffectiveDateYear"/> - <xsl:value-of select="$TableChangeReason"/>)</fo:block>				
			</xsl:when>
			<xsl:otherwise>
				<fo:block space-after="1mm" font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Mailsort Parameters</fo:block>								
			</xsl:otherwise>
		</xsl:choose>

		<fo:table table-layout="fixed" width="100%">
			<xsl:choose>
				<xsl:when test="$USE-APIDemo='true'">
					<fo:table-column column-width="6cm"/>
					<fo:table-column column-width="15cm"/>					
				</xsl:when>
				<xsl:otherwise>
					<fo:table-column column-width="proportional-column-width(2)"/>
					<fo:table-column column-width="proportional-column-width(5)"/>
				</xsl:otherwise>
			</xsl:choose>
			<fo:table-body>
				<!-- FOR DEBUGGING PURPOSES -->				
				<!-- <xsl:if test="not($lowsort)">
					<xsl:call-template name="PlanningParametersRow">
						<xsl:with-param name="Title" select="'Direct/Residue Order (debug)'"/>
						<xsl:with-param name="Value" select="$DirectResidueAlternateOrder"/>
					</xsl:call-template>
				</xsl:if>
								
				<xsl:call-template name="PlanningParametersRow">
					<xsl:with-param name="Title" select="'Low Sort (debug)'"/>
					<xsl:with-param name="Value" select="$lowsort"/>
				</xsl:call-template>
				
				<xsl:call-template name="PlanningParametersRow">
					<xsl:with-param name="Title" select="'Mailmark (debug)'"/>
					<xsl:with-param name="Value" select="$mailmark"/>
				</xsl:call-template>
				
				<xsl:call-template name="PlanningParametersRow">
					<xsl:with-param name="Title" select="'Regional Sort (debug)'"/>
					<xsl:with-param name="Value" select="$regionalsort"/>
				</xsl:call-template>

				<xsl:if test="$ukmail">
					<xsl:call-template name="PlanningParametersRow">
						<xsl:with-param name="Title" select="'UK Mail (debug)'"/>
						<xsl:with-param name="Value" select="$ukmail"/>
					</xsl:call-template>
				</xsl:if>					
				<xsl:if test="$tnt">
					<xsl:call-template name="PlanningParametersRow">
						<xsl:with-param name="Title" select="'Whistl (debug)'"/>
						<xsl:with-param name="Value" select="$tnt"/>
					</xsl:call-template>
				</xsl:if>					
				<xsl:if test="$citipost">
					<xsl:call-template name="PlanningParametersRow">
						<xsl:with-param name="Title" select="'Citipost (debug)'"/>
						<xsl:with-param name="Value" select="$citipost"/>
					</xsl:call-template>
				</xsl:if>					
				<xsl:if test="$securedmail">
					<xsl:call-template name="PlanningParametersRow">
						<xsl:with-param name="Title" select="'Secured Mail (debug)'"/>
						<xsl:with-param name="Value" select="$securedmail"/>
					</xsl:call-template>
				</xsl:if>					
				
				<xsl:call-template name="PlanningParametersRow">
					<xsl:with-param name="Title" select="'DSA (debug)'"/>
					<xsl:with-param name="Value" select="$dsa"/>
				</xsl:call-template>
				 -->
				<!-- START OF PROPER PARAMETERS -->
				<xsl:call-template name="PlanningParametersRow">
					<xsl:with-param name="Title" select="'Sort Service'"/>
			   	<xsl:with-param name="Value" select="$mailsortservice"/>
				</xsl:call-template>				

				<xsl:if test="not($UnSorted)">
					<xsl:call-template name="PlanningParametersRow">
						<xsl:with-param name="Title" select="'Minimum Selection'"/>
						<xsl:with-param name="Value" select="$MinimumSelection"/>
					</xsl:call-template>		
				</xsl:if>
				
				<xsl:call-template name="PlanningParametersRow">
					<xsl:with-param name="Title" select="'Format'"/>
					<xsl:with-param name="Value" select="$format"/>
				</xsl:call-template>

				<xsl:if test="not($UnSorted)">
					<xsl:choose>
						<xsl:when test="$MailType = 'Advertising+Sustainable'">
							<xsl:call-template name="PlanningParametersRow">
								<xsl:with-param name="Title" select="'Mail Type'"/>
								<xsl:with-param name="Value" select="'Advertising'"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="PlanningParametersRow">
								<xsl:with-param name="Title" select="'Mail Type'"/>
								<xsl:with-param name="Value" select="$MailType"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				
				<xsl:call-template name="PlanningParametersRow">
					<xsl:with-param name="Title">
						<xsl:if test="$mixedweight"><xsl:value-of select="'Avg. '"/></xsl:if>
						<xsl:value-of select="'Item Weight'"/>
					</xsl:with-param>
					<xsl:with-param name="Value" select="$ItemWeight"/>
				</xsl:call-template>
				
				<xsl:if test="$UnSorted">
					<xsl:call-template name="PlanningParametersRow">
						<xsl:with-param name="Title" select="concat($ContainerDescriptionSingular, ' Weight')"/>
						<xsl:with-param name="Value" select="$BagWeight"/>
					</xsl:call-template>
				</xsl:if>				
				
				<xsl:call-template name="PlanningParametersRow">
					<xsl:with-param name="Title" select="$ContainerDescriptionMaxItems"/>
					<xsl:with-param name="Value" select="$MaxItemsPerBag"/>
				</xsl:call-template>
				
		        <xsl:call-template name="PlanningParametersRow">
        		  <xsl:with-param name="Title" select="'Container'"/>
          			<xsl:with-param name="Value" select="$ContainerDescription"/>
        		</xsl:call-template>

				<xsl:if test="not($UnSorted)">
        			<xsl:call-template name="PlanningParametersRow">
          				<xsl:with-param name="Title" select="'Sustainability'"/>
          				<xsl:with-param name="Value" select="$Sustainability"/>
        			</xsl:call-template>
				</xsl:if>					
				
				<xsl:if test="($ukmail)">
					<xsl:call-template name="PlanningParametersRow">
						<xsl:with-param name="Title" select="'Collection Date'"/>
						<xsl:with-param name="Value" select="$DSACollectionDate"/>
					</xsl:call-template>				
				</xsl:if>

				<xsl:if test="not($tnt)">
					<xsl:if test="$MailingRegion">
						<xsl:call-template name="PlanningParametersRow">
							<xsl:with-param name="Title" select="'Mailing Region'"/>
							<xsl:with-param name="Value">
								<xsl:value-of select="$MailingRegion"/> - <xsl:value-of select="$MailingRegionDescription"/>
							</xsl:with-param>
						</xsl:call-template>		
					</xsl:if>
				</xsl:if>
				
				<xsl:if test="$ukmail">
					<xsl:call-template name="PlanningParametersRow">
						<xsl:with-param name="Title" select="'UK Mail Account Number'"/>
						<xsl:with-param name="Value" select="$DSAAccountNumberAKAClient"/>
					</xsl:call-template>				
				</xsl:if>
				
				<xsl:if test="$zonal">
					<xsl:call-template name="PlanningParametersRow">
						<xsl:with-param name="Title">
							<xsl:choose>
								<xsl:when test="$ukmail">UK Mail Zonal</xsl:when>
								<xsl:when test="$tnt">Whistl Zonal</xsl:when>
								<xsl:when test="$securedmail">Secured Mail Zonal</xsl:when>
								<xsl:otherwise>???</xsl:otherwise>
							</xsl:choose>							
						</xsl:with-param>
						<xsl:with-param name="Value" select="$DSAZonal"/>
					</xsl:call-template>
				</xsl:if>
				
				<xsl:call-template name="PlanningParametersRow">
					<xsl:with-param name="Title">
						<xsl:choose>
							<xsl:when test="$dsa">DSA Reference</xsl:when>
							<xsl:otherwise>Mailing Reference</xsl:otherwise>
						</xsl:choose>							
					</xsl:with-param>
					<xsl:with-param name="Value" select="$MailingReference"/>					
				</xsl:call-template>
			</fo:table-body>
		</fo:table>
	</xsl:template>

	<xsl:template name="PlanningPresortationStatistics">
		<fo:block space-after="1mm" font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Presortation Statistics
		</fo:block>
		<fo:table table-layout="fixed">
			<fo:table-column column-width="50mm"/>
			<fo:table-column column-width="30mm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding-left="2mm">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal">Postcode
							Match</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="1mm" text-align="right">
							<xsl:value-of select="format-number($postcodematch,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal">Post Town
							Match</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="1mm" text-align="right">
							<xsl:value-of select="format-number($posttownmatch,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal"/>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="1mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number($mailsortitems,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal">Ineligible for Mailsort</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="1mm" text-align="right">
							<xsl:value-of select="format-number($standardtariffitems,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal"/>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="1mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number($totalitems,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>		
	</xsl:template>

	<xsl:template name="PlanningPostcodeStatistics">
		<xsl:param name="mixedWeight"/>		
		<fo:block space-before="6mm" space-after="1mm" font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Postcode Statistics</fo:block>
		<fo:table table-layout="fixed">
			<xsl:choose>
				<xsl:when test="($USE-APIDemo = 'true')">
					<xsl:choose>
						<xsl:when test="$mixedWeight = 'true'">
							<fo:table-column column-width="50mm"/>
							<fo:table-column column-width="30mm"/>																					
						</xsl:when>
						<xsl:otherwise>
							<fo:table-column column-width="30mm"/>
							<fo:table-column column-width="20mm"/>														
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<fo:table-column column-width="proportional-column-width(2)"/>
					<fo:table-column column-width="proportional-column-width(1)"/>					
				</xsl:otherwise>
			</xsl:choose>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding-left="2mm">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal">Fully
							Postcoded</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="1mm" text-align="right">
							<xsl:value-of select="$fullypostcoded"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal">Outbound
							Only</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="1mm" text-align="right">
							<xsl:value-of select="$outboundonly"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal"/>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="1mm" text-align="right" font-weight="bold">
							<xsl:value-of select="$postcodetotal"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal">No
							Postcode</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="1mm" text-align="right">
							<xsl:value-of select="$nopostcode"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>		
	</xsl:template>

	<xsl:template name="PlanningSortStatsRow">
		<xsl:param name="description"/>
		<xsl:param name="items"/>    			
		<xsl:param name="number"/>    			
		<xsl:param name="bags"/>
		<fo:table-row>
			<fo:table-cell padding-left="2mm">
				<fo:block font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal"><xsl:value-of select="$description"/></fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block text-align="right">
					<xsl:value-of select="format-number($items,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block text-align="right">
					<xsl:choose>
						<xsl:when test="$TotalQualify = 0">N/A</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="format-number(100 * $items div $TotalQualify,'###,###,##0.00')"/>							
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block text-align="right">
					<xsl:value-of select="format-number($number,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block text-align="right">
					<xsl:value-of select="format-number($bags,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		
	</xsl:template>

	<xsl:template name="PlanningSortStats">
		<fo:table space-before="5mm" table-layout="fixed">
			<fo:table-column column-width="50mm"/>
			<fo:table-column column-width="30mm"/>
			<xsl:choose>
				<xsl:when test="$mixedweight"><fo:table-column column-width="30mm"/></xsl:when>
				<xsl:otherwise><fo:table-column column-width="60mm"/></xsl:otherwise>
			</xsl:choose>						
			<fo:table-column column-width="25mm"/>
			<fo:table-column column-width="25mm"/>
			<fo:table-body>
				<!-- First Header Row -->
				<fo:table-row>
					<fo:table-cell>
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Sortation Statistics</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold" text-align="right">Items</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold" text-align="right">%Match</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold" text-align="right">Number</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold" text-align="right"><xsl:value-of select="$ContainerDescription"/>
							</fo:block>
					</fo:table-cell>
				</fo:table-row>

				<!-- Second Detail Rows -->
				<xsl:choose>
					<xsl:when test="($lowsort)">
						<xsl:call-template name="PlanningSortStatsRow">
							<xsl:with-param name="description" select="'Eligible Selections'"/>
							<xsl:with-param name="items" select="$resitems"/>
							<xsl:with-param name="number" select="$residueselections"/>
							<xsl:with-param name="bags" select="$residuebags"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="PlanningSortStatsRow">
							<xsl:with-param name="description" select="'Direct Selections'"/>
							<xsl:with-param name="items" select="$diritems"/>
							<xsl:with-param name="number" select="$directselections"/>
							<xsl:with-param name="bags" select="$directbags"/>
						</xsl:call-template>

						<xsl:call-template name="PlanningSortStatsRow">
							<xsl:with-param name="description" select="'Residue Selections'"/>
							<xsl:with-param name="items" select="$resitems"/>
							<xsl:with-param name="number" select="$residueselections"/>
							<xsl:with-param name="bags" select="$residuebags"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
				
				<!-- Third Total Row -->
				<fo:table-row>
					<fo:table-cell padding-left="2mm">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal"/>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="1mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number($TotalQualify,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="1mm" text-align="right"/>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="1mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number($directselections + $residueselections,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="1mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number($directbags + $residuebags,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
 
	<xsl:template name="PlanningDPSStats">
		<xsl:param name="mixedWeight"></xsl:param>
		<fo:block space-before="6mm" space-after="1mm" font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">DPS Statistics</fo:block>
		<fo:table table-layout="fixed" width="100%">
			<xsl:choose>
				<xsl:when test="($USE-APIDemo = 'true')">
					<xsl:choose>
						<xsl:when test="$mixedWeight = 'true'">
							<fo:table-column column-width="4cm"/>
							<fo:table-column column-width="28mm"/>																					
						</xsl:when>
						<xsl:otherwise>
							<fo:table-column column-width="2cm"/>
							<fo:table-column column-width="22mm"/>					
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<fo:table-column column-width="proportional-column-width(1)"/>
					<fo:table-column column-width="proportional-column-width(1)"/>
				</xsl:otherwise>
			</xsl:choose>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding-left="2mm">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal">Matched</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2mm">
						<fo:block space-before="1mm" text-align="right">
							<xsl:value-of select="format-number($DPSMatchItems,'##0.00')"/>%
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal">Default</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="1mm" text-align="right">
							<xsl:value-of select="format-number($DPSDefaultsItems,'##0.00')"/>%
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal"/>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="1mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number($DPSMatchItems + $DPSDefaultsItems,'##0.00')"/>%
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal">No DPS</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="1mm" text-align="right">
							<xsl:value-of select="format-number(100 - ($DPSMatchItems + $DPSDefaultsItems),'##0.00')"/>%
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template name="PlanningCosts">
		<fo:table space-before="5mm" table-layout="fixed">
			<fo:table-column column-width="50mm"/>
			<fo:table-column column-width="30mm"/>
			<xsl:choose>
				<xsl:when test="$mixedweight"><fo:table-column column-width="30mm"/></xsl:when>
				<xsl:otherwise><fo:table-column column-width="60mm"/></xsl:otherwise>
			</xsl:choose>			
			<fo:table-column column-width="50mm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Costs</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Items</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Rate</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">£</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<xsl:choose>
					<xsl:when test="not($lowsort)">
						<fo:table-row>
							<fo:table-cell padding-left="2mm" text-align="left">
								<fo:block font-family="{$h5-family}" font-size="{$h5-size}">Direct Items</fo:block>
							</fo:table-cell>
							<fo:table-cell text-align="right">
								<fo:block>
									<xsl:value-of select="format-number($diritems,'###,###,##0')"/>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell text-align="right">
								<fo:block>
									<xsl:value-of select="format-number($directrate,'##0.00')"/>p</fo:block>
							</fo:table-cell>
							<fo:table-cell text-align="right">
								<fo:block>
									<xsl:value-of select="format-number($dirdiscount,'###,###,##0.00')"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</xsl:when>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="not($lowsort)">
						<fo:table-row>
							<fo:table-cell padding-left="2mm" text-align="left">
								<fo:block font-family="{$h5-family}" font-size="{$h5-size}">Residue Items</fo:block>
							</fo:table-cell>
							<fo:table-cell text-align="right">
								<fo:block>
									<xsl:value-of select="format-number($resitems,'###,###,##0')"/>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell text-align="right">
								<fo:block>
									<xsl:value-of select="format-number($residuerate,'##0.00')"/>p</fo:block>
							</fo:table-cell>
							<fo:table-cell text-align="right">
								<fo:block>
									<xsl:value-of select="format-number($resdiscount,'###,###,##0.00')"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</xsl:when>
					<xsl:otherwise>
						<fo:table-row>
							<fo:table-cell padding-left="2mm" text-align="left">
								<fo:block font-family="{$h5-family}" font-size="{$h5-size}">Eligible Items</fo:block>
							</fo:table-cell>
							<fo:table-cell text-align="right">
								<fo:block>
									<xsl:value-of select="format-number($resitems,'###,###,##0')"/>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell text-align="right">
								<fo:block>
									<xsl:value-of select="format-number($residuerate,'##0.00')"/>p</fo:block>
							</fo:table-cell>
							<fo:table-cell text-align="right">
								<fo:block>
									<xsl:value-of select="format-number($resdiscount,'###,###,##0.00')"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="$MailType = 'Advertising'">
						<fo:table-row>
							<fo:table-cell padding-left="2mm" text-align="left">
								<fo:block space-before="1mm" color="darkgrey">
									<xsl:value-of select="$levy"/>%
								Levy</fo:block>
							</fo:table-cell>
							<fo:table-cell number-columns-spanned="2">
								<fo:block space-before="1mm"/>
							</fo:table-cell>
							<fo:table-cell text-align="right">
								<fo:block space-before="1mm" color="darkgrey">
									<xsl:value-of select="format-number($levycost,'###,###,##0.00')"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell number-columns-spanned="3">
								<fo:block space-before="1mm"/>
							</fo:table-cell>
							<fo:table-cell text-align="right">
								<fo:block space-before="1mm" font-weight="bold">
									<xsl:value-of select="format-number($x9PlusLevy,'###,###,##0.00')"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</xsl:when>
					<xsl:otherwise>
						<fo:table-row>
							<fo:table-cell number-columns-spanned="3">
								<fo:block space-before="1mm"/>
							</fo:table-cell>
							<fo:table-cell text-align="right">
								<fo:block space-before="1mm" font-weight="bold">
									<xsl:value-of select="format-number($x9,'###,###,##0.00')"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</xsl:otherwise>
				</xsl:choose>
			</fo:table-body>
		</fo:table>
	</xsl:template>

	<xsl:template name="PlanningSavings">
		<fo:table space-before="5mm" table-layout="fixed">
			<fo:table-column column-width="50mm"/>
			<fo:table-column column-width="30mm"/>
			<fo:table-column column-width="60mm"/>
			<fo:table-column column-width="50mm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Savings</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Items</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Rate</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Net £</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm" text-align="left">
						<fo:block font-family="{$h5-family}" font-size="{$h5-size}">Standard Tariff Cost</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block>
							<xsl:value-of select="format-number($TotalQualify,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block>
							<xsl:value-of select="format-number($STRateVRD,'##0.00')"/>p</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block>
							<xsl:value-of select="format-number($STCostVRD,'###,###,##0.00')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm" text-align="left">
						<fo:block font-family="{$h5-family}" font-size="{$h5-size}">Saving</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block/>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block>
							<xsl:if test="$STCostVRD != '0'">
								<xsl:value-of select="format-number(($STCostVRD - $x9) div $STCostVRD * 100,'##0.00')"/>%					
							</xsl:if>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block>
							<xsl:value-of select="format-number($STCostVRD - $x9,'###,###,##0.00')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>

	<xsl:template name="PlanningStandardTariff">
		<fo:table space-before="5mm" table-layout="fixed">
			<fo:table-column column-width="50mm"/>
			<fo:table-column column-width="30mm"/>
			<fo:table-column column-width="60mm"/>
			<fo:table-column column-width="50mm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Standard Tariff</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Items</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Tariff</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">£</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm" text-align="left">
						<fo:block font-family="{$h5-family}" font-size="{$h5-size}">Europe</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block>
							<xsl:value-of select="format-number($EuropeItems,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block>
							<xsl:value-of select="$EuropeRate"/>p</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block>
							<xsl:value-of select="format-number($EuropeCost,'###,###,##0.00')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm" text-align="left">
						<fo:block font-family="{$h5-family}" font-size="{$h5-size}">Zone 1</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block>
							<xsl:value-of select="format-number($Zone1Items,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block>
							<xsl:value-of select="$Zone1Rate"/>p</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block>
							<xsl:value-of select="format-number($Zone1Cost,'###,###,##0.00')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm" text-align="left">
						<fo:block font-family="{$h5-family}" font-size="{$h5-size}">Zone 2</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block>
							<xsl:value-of select="format-number($Zone2Items,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block>
							<xsl:value-of select="$Zone2Rate"/>p</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block>
							<xsl:value-of select="format-number($Zone2Cost,'###,###,##0.00')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm" text-align="left">
						<fo:block font-family="{$h5-family}" font-size="{$h5-size}">UK Unmatched Items</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block>
							<xsl:value-of select="format-number($UKUnmatchedItems,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block>
							<xsl:value-of select="$UKUnmatchedRate"/>p</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block>
							<xsl:value-of select="format-number($UKUnmatchedCost,'###,###,##0.00')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm" text-align="left">
						<fo:block font-family="{$h5-family}" font-size="{$h5-size}">UK Withdrawn Items</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block>
							<xsl:value-of select="format-number($UKWithdrawnItems,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block><xsl:value-of select="$UKWithdrawnRate"/>p</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block>
							<xsl:value-of select="format-number($UKWithdrawnCost,'###,###,##0.00')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm" text-align="left">
						<fo:block font-family="{$h5-family}" font-size="{$h5-size}"/>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block font-weight="bold">
							<xsl:value-of select="format-number($EuropeItems + $Zone1Items + $Zone2Items + $UKUnmatchedItems + $UKWithdrawnItems,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block/>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block font-weight="bold">
							<xsl:value-of select="format-number($EuropeCost + $Zone1Cost + $Zone2Cost + $UKUnmatchedCost + $UKWithdrawnCost,'###,###,##0.00')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>

	<xsl:template name="PlanningVolumes">
		<fo:block space-before="5mm" font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Volumes</fo:block>
		<fo:table table-layout="fixed">
			<fo:table-column column-width="50mm"/>
			<fo:table-column column-width="30mm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell padding-left="2mm" text-align="left">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}">Mailsort Items</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block space-before="1mm">
							<xsl:value-of select="format-number($mailsortitems,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm" text-align="left">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}">Standard Tariff Items</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block space-before="1mm">
							<xsl:value-of select="format-number($standardtariffitems,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell padding-left="2mm" text-align="left">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}">Total Items</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block space-before="1mm" font-weight="bold">
							<xsl:value-of select="format-number($totalitems,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>	

	<xsl:template name="OutputFile">
		<xsl:param name="Title"/>
		<xsl:param name="Filepath"/>
		<xsl:param name="Directory"/>
		<fo:table-row>
			<fo:table-cell padding-left="2mm">
				<fo:block font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold"><xsl:value-of select="$Title"/></fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block font-weight="normal">
					<fo:inline color="blue" font-weight="bold"><xsl:value-of select="$Directory"/>\</fo:inline><xsl:value-of select="$Filepath"/>
				</fo:block>																				
			</fo:table-cell>
		</fo:table-row>		
	</xsl:template>

	<xsl:template name="OutputDirectory">
		<xsl:param name="Title"/>
		<xsl:param name="Filepath"/>
		<fo:table-row>
			<fo:table-cell padding-left="2mm">
				<fo:block font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold" color="blue"><xsl:value-of select="$Title"/></fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block font-weight="normal">
					<xsl:value-of select="$Filepath"/>
				</fo:block>																				
			</fo:table-cell>
		</fo:table-row>		
	</xsl:template>
	
	<xsl:template name="Directories">
		<fo:block space-before="5mm" space-after="1mm" font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Directories</fo:block>
		<fo:table table-layout="fixed" width="100%">
			<xsl:choose>
				<xsl:when test="($USE-APIDemo = 'true')">
					<fo:table-column column-width="3.5cm"/>		<!-- was 6cm -->
					<fo:table-column column-width="17.5cm"/>	<!-- was 15cm -->			
				</xsl:when>
				<xsl:otherwise>
					<fo:table-column column-width="proportional-column-width(2)"/>
					<fo:table-column column-width="proportional-column-width(5)"/>
				</xsl:otherwise>
			</xsl:choose>
			<fo:table-body>
				<xsl:for-each select="$Directories">
					<xsl:call-template name="OutputDirectory">
						<xsl:with-param name="Title" select="Description"/>						
						<xsl:with-param name="Filepath" select="Filepath"/>						
					</xsl:call-template>						
				</xsl:for-each>				
			</fo:table-body>
		</fo:table>
	</xsl:template>

	<xsl:template name="PlanningOutputFiles">
		<xsl:if test="$onepost or $ukmail or $tnt">
			<xsl:call-template name="Directories"></xsl:call-template>
		</xsl:if>
		
		<fo:block space-before="5mm" space-after="1mm" font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Output Files</fo:block>
		<fo:table table-layout="fixed" width="100%">
			<xsl:choose>
				<xsl:when test="($USE-APIDemo = 'true')">
					<fo:table-column column-width="3.5cm"/>		<!-- was 6cm -->
					<fo:table-column column-width="17.5cm"/>	<!-- was 15cm -->			
				</xsl:when>
				<xsl:otherwise>
					<fo:table-column column-width="proportional-column-width(2)"/>
					<fo:table-column column-width="proportional-column-width(5)"/>
				</xsl:otherwise>
			</xsl:choose>
			<fo:table-body>
				<xsl:if test="not($dsa)">
					<fo:table-row>
						<fo:table-cell padding-left="2mm">
							<fo:block font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal">Bag Labels</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block font-weight="bold">
								<xsl:value-of select="$BagLabelFile"/>
							</fo:block>																				
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
				<xsl:if test="($securedmail = 'true') or ($citipost = 'true')"> 
					<fo:table-row>
						<fo:table-cell padding-left="2mm">
							<fo:block font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal">DocketHub File</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block font-weight="bold">
								<xsl:value-of select="$BagLabelFile"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:if>
				<xsl:if test="$onepost or $ukmail or $tnt">
					<xsl:for-each select="$OnePostMainFiles">
						<xsl:call-template name="OutputFile">
							<xsl:with-param name="Title" select="Description"/>						
							<xsl:with-param name="Filepath" select="FileName"/>
							<xsl:with-param name="Directory" select="Directory"/>
						</xsl:call-template>						
					</xsl:for-each>
								
					<xsl:for-each select="$OnePostBagFiles">
						<xsl:if test="position() = 1">
							<xsl:call-template name="OutputFile">
								<xsl:with-param name="Title" select="Description"/>						
								<xsl:with-param name="Filepath" select="FileName"/>						
								<xsl:with-param name="Directory" select="Directory"/>
							</xsl:call-template>						
						</xsl:if>
						<xsl:if test="position() > 1">
							<xsl:call-template name="OutputFile">
								<xsl:with-param name="Title" select="''"/>						
								<xsl:with-param name="Filepath" select="FileName"/>						
								<xsl:with-param name="Directory" select="Directory"/>
							</xsl:call-template>						
						</xsl:if>
					</xsl:for-each>
					
					<xsl:for-each select="$OnePostOtherFiles">
							<xsl:if test="position() = 1">
								<xsl:call-template name="OutputFile">
									<xsl:with-param name="Title" select="Description"/>														
									<xsl:with-param name="Filepath" select="FileName"/>
									<xsl:with-param name="Directory" select="Directory"/>
								</xsl:call-template>
							</xsl:if>
						<xsl:if test="position() > 1">
							<xsl:call-template name="OutputFile">
								<xsl:with-param name="Title" select="''"/>														
								<xsl:with-param name="Filepath" select="FileName"/>
								<xsl:with-param name="Directory" select="Directory"/>
							</xsl:call-template>
						</xsl:if>
										
					</xsl:for-each>					
				</xsl:if>
			</fo:table-body>
		</fo:table>
	</xsl:template>

	<xsl:template name="PlanningWeights">
		<fo:table space-before="5mm"  table-layout="fixed" width="100%">
			<xsl:choose>
				<xsl:when test="($USE-APIDemo = 'true')">
					<fo:table-column column-width="2.5cm"/>
					<fo:table-column column-width="4cm"/>
					<fo:table-column column-width="4cm"/>
					<fo:table-column column-width="4cm"/>									
				</xsl:when>
				<xsl:otherwise>
					<fo:table-column column-width="proportional-column-width(2)"/>
					<fo:table-column column-width="proportional-column-width(2)"/>
					<fo:table-column column-width="proportional-column-width(2)"/>
					<fo:table-column column-width="proportional-column-width(2)"/>				
				</xsl:otherwise>
			</xsl:choose>
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell>
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Weight Analysis</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Items</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2mm" text-align="right">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Actual Weight</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Assumed Weight</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<xsl:for-each select="WeightStatistics/Weight">
					<fo:table-row>
						<fo:table-cell padding-left="2mm">
							<fo:block font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal">
								<xsl:value-of select="Grams"/>g</fo:block>
						</fo:table-cell>
						<fo:table-cell text-align="right">
							<fo:block font-weight="normal">
								<xsl:value-of select="format-number(Items,'###,###,##0')"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell text-align="right">
							<fo:block font-weight="normal">
								<xsl:value-of select="format-number(AcctWeight,'###,###,##0')"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell text-align="right">
							<fo:block font-weight="normal">
								<xsl:value-of select="format-number(AssuWeight,'###,###,##0')"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>					
				</xsl:for-each>
				
				<fo:table-row>
					<fo:table-cell padding-left="2mm">
						<fo:block font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold"/>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block font-weight="bold">
							<xsl:value-of select="format-number(sum(WeightStatistics/Weight/Items),'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block font-weight="bold">
							<xsl:value-of select="format-number(sum(WeightStatistics/Weight/AcctWeight),'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block font-weight="bold">
							<xsl:value-of select="format-number(sum(WeightStatistics/Weight/AssuWeight),'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>		
			</fo:table-body>
		</fo:table>
	</xsl:template>

	<xsl:template name="PlanningWhistlHeaderRow">
		<xsl:param name="header"/>
		<xsl:param name="value"/>
		<fo:inline font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold"><xsl:value-of select="$header"/>: </fo:inline><fo:inline font-family="{$h1-family}" font-size="{$h1-size}" font-weight="bold"><xsl:value-of select="$value"/></fo:inline>
	</xsl:template>

	<xsl:template name="PlanningWhistlService">
		<xsl:call-template name="PlanningWhistlHeaderRow">
			<xsl:with-param name="header">Whistl Service</xsl:with-param>
			<xsl:with-param name="value" select="$WhistlService"/>
		</xsl:call-template>							
	</xsl:template>

	<xsl:template name="PlanningWhistlPO">
		<xsl:call-template name="PlanningWhistlHeaderRow">
			<xsl:with-param name="header">Whistl PO</xsl:with-param>
			<xsl:with-param name="value" select="$WhistlPO"/>
		</xsl:call-template>							
	</xsl:template>
	
	<xsl:template name="PlanningWhistl">
		<!-- Complicated by the requirement to Left Justify for Mixed Weight but indent for Fixed Weight. Also note the space-before is different for Mixed/Fixed Weight -->
		<xsl:choose>
			<xsl:when test="$mixedweight">
				<fo:block space-before="5mm" text-indent="0mm">
					<xsl:call-template name="PlanningWhistlService"></xsl:call-template>
				</fo:block>
				
				<fo:block space-before="2mm" text-indent="0mm">
					<xsl:call-template name="PlanningWhistlPO"></xsl:call-template>
				</fo:block>
			</xsl:when>
			<xsl:otherwise>
				<fo:block space-before="15mm" text-indent="15mm">
					<xsl:call-template name="PlanningWhistlService"></xsl:call-template>
				</fo:block>
				
				<fo:block space-before="2mm" text-indent="15mm">
					<xsl:call-template name="PlanningWhistlPO"></xsl:call-template>
				</fo:block>				
			</xsl:otherwise>
		</xsl:choose>
				
		<fo:table space-before="2mm" space-after="15mm" table-layout="fixed">
			<xsl:choose>
				<xsl:when test="$mixedweight"><fo:table-column column-width="0mm"/></xsl:when>					
				<xsl:otherwise><fo:table-column column-width="20mm"/></xsl:otherwise>				<!-- Filler to push over 35mm -->
			</xsl:choose>
			<fo:table-column column-width="25mm"/>	<!-- Client -->
			<fo:table-column column-width="35mm"/>	<!-- Mailing House -->
			<fo:table-column column-width="30mm"/>	<!-- Segmentation -->
			<fo:table-column column-width="30mm"/>	<!-- Collection -->
			<fo:table-column column-width="30mm"/>	<!-- Depot -->
			<fo:table-column column-width="35mm"/>	<!-- Filler -->
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell>
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold"></fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Client</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Mailing House</fo:block>
					</fo:table-cell>
					<fo:table-cell padding-left="2mm" text-align="center">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Segementation</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Collection</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Depot</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold"></fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell><fo:block/></fo:table-cell>		<!-- Filler -->
					<fo:table-cell>
						<fo:block>
							<xsl:value-of select="$DSAAccountNumberAKAClient"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center">
						<fo:block>
							<xsl:value-of select="$WhistlMailingHouse"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center">
						<fo:block>
							<xsl:value-of select="$WhistlSegmentCode"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center">
						<fo:block>
							<xsl:value-of select="$DSACollectionDate"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center">
						<fo:block>
							<xsl:value-of select="$WhistlDepot"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell><fo:block/></fo:table-cell>		<!-- Filler -->
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<!-- ++++++++++++++++++++++++++++++++++            Elibigle Selection Listings - Not Mixed Weight              ++++++++++++++++++++++++++++  -->
	
	<xsl:template name="ListingHeaderPage">
		<!--  First Duplicate the Planning Parameters output to the first page -->
		<xsl:call-template name="PlanningParameters"/>				
		<xsl:call-template name="PlanningVolumes"/>
		<xsl:if test="$mailmark">
			<xsl:call-template name="ListingParametersMailmark"/>			
		</xsl:if>
		
		<!-- Only call PlanningOutputFiles from here for sort reports that have not moved to displaying the output files on their own page -->
		<xsl:if test="$securedmail or $citipost">
			<xsl:call-template name="PlanningOutputFiles"/>			
		</xsl:if>
	</xsl:template>
				
	<xsl:template name="ListingAlternate">
		<fo:page-sequence master-reference="A4Portrait">
			<xsl:call-template name="Header">
				<xsl:with-param name="Title" select="'Eligible Selections'"/>
				<xsl:with-param name="USE-APIDemo" select="$USE-APIDemo"/>				
			</xsl:call-template>
			<xsl:call-template name="Footer"/>
			<fo:flow flow-name="xsl-region-body" font-family="{$d1-family}" font-size="{$d1-size}" font-weight="normal">
				<xsl:call-template name="ListingAll"/>
			</fo:flow>
		</fo:page-sequence>		
	</xsl:template>
	
	<xsl:template name="ListingLowSort">
		<xsl:call-template name="ListingAlternate"></xsl:call-template>
	</xsl:template>
	
	<xsl:template name="ListingResidueDirect">
		<fo:page-sequence master-reference="A4Portrait">
			<xsl:call-template name="Header">
				<xsl:with-param name="Title" select="'Residue Selections'"/>
				<xsl:with-param name="USE-APIDemo" select="$USE-APIDemo"/>				
			</xsl:call-template>
			<xsl:call-template name="Footer"/>
			<fo:flow flow-name="xsl-region-body" font-family="{$d1-family}" font-size="{$d1-size}" font-weight="normal">
				<xsl:call-template name="ListingResidue">
					<xsl:with-param name="Totals" select="'false'"/>										
				</xsl:call-template>
			</fo:flow>
		</fo:page-sequence>		
		
		<fo:page-sequence master-reference="A4Portrait">
			<xsl:call-template name="Header">
				<xsl:with-param name="Title" select="'Direct Selections'"/>
				<xsl:with-param name="USE-APIDemo" select="$USE-APIDemo"/>				
			</xsl:call-template>
			<xsl:call-template name="Footer"/>
			<fo:flow flow-name="xsl-region-body" font-family="{$d1-family}" font-size="{$d1-size}" font-weight="normal">
				<xsl:call-template name="ListingDirect">
					<xsl:with-param name="Totals" select="'true'"/>										
				</xsl:call-template>
			</fo:flow>
		</fo:page-sequence>		
	</xsl:template>
	
	<xsl:template name="ListingDirectResidue">
		<fo:page-sequence master-reference="A4Portrait">
			<xsl:call-template name="Header">
				<xsl:with-param name="Title" select="'Direct Selections'"/>
				<xsl:with-param name="USE-APIDemo" select="$USE-APIDemo"/>				
			</xsl:call-template>
			<xsl:call-template name="Footer"/>
			<fo:flow flow-name="xsl-region-body" font-family="{$d1-family}" font-size="{$d1-size}" font-weight="normal">
				<xsl:call-template name="ListingDirect">
					<xsl:with-param name="Totals" select="'false'"/>
				</xsl:call-template>
			</fo:flow>
		</fo:page-sequence>
		
		<fo:page-sequence master-reference="A4Portrait">
			<xsl:call-template name="Header">
				<xsl:with-param name="Title" select="'Residue Selections'"/>
				<xsl:with-param name="USE-APIDemo" select="$USE-APIDemo"/>				
			</xsl:call-template>
			<xsl:call-template name="Footer"/>
			<fo:flow flow-name="xsl-region-body" font-family="{$d1-family}" font-size="{$d1-size}" font-weight="normal">
				<xsl:call-template name="ListingResidue">
					<xsl:with-param name="Totals" select="'true'"/>					
				</xsl:call-template>
			</fo:flow>
		</fo:page-sequence>		
	</xsl:template>
	
	<xsl:template name="ListingRow">
		<xsl:param name="spaceafter"/>
    	<xsl:param name="residue"/>    	
    	<fo:table-row>
			<fo:table-cell>
				<fo:block space-after="{$spaceafter}" space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal">
					<xsl:choose>
						<xsl:when test="$residue or $lowsort">
							<xsl:value-of select="substring(Code,1,3)"/>			<!-- For residue we don't print the 00 at end of selection code -->		
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="Code"/>									
						</xsl:otherwise>
					</xsl:choose>					
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-after="{$spaceafter}" space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}">
					<xsl:value-of select="Office"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-after="{$spaceafter}" space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}">
					<xsl:value-of select="Area"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-after="{$spaceafter}" space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}">
					<!-- <xsl:value-of select="Code"/> -->
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-after="{$spaceafter}" space-before="1mm" text-align="right">
					<xsl:value-of select="format-number(Items,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-after="{$spaceafter}" space-before="1mm" text-align="right">
					<xsl:value-of select="format-number(AccumItems,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-after="{$spaceafter}" space-before="1mm" text-align="right">
					<xsl:value-of select="format-number(Bags,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-after="{$spaceafter}" space-before="1mm" text-align="right">
					<xsl:value-of select="format-number(AccumBags,'###,###,##0')"/>				
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-after="{$spaceafter}" space-before="1mm" text-align="right" color="red"> 
					<xsl:value-of select="Selection/Islands"/>
				</fo:block>
			</fo:table-cell>												
		</fo:table-row>
	</xsl:template>
	
	<xsl:template name="ListingColumns">
		<fo:table-column column-width="12mm"/>
		<fo:table-column column-width="40mm"/>
		<fo:table-column column-width="40mm"/>
		<fo:table-column column-width="3mm"/>
		<fo:table-column column-width="20mm"/>
		<fo:table-column column-width="20mm"/>
		<fo:table-column column-width="15mm"/>
		<fo:table-column column-width="15mm"/>
		<fo:table-column column-width="5mm"/>
		<fo:table-column column-width="5mm"/>		
	</xsl:template>
	
	<xsl:template name="ListingPageHeader">
		<fo:table-header>
			<fo:table-row>
				<fo:table-cell text-align="left" number-columns-spanned="4">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Selection</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Items</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Accum</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold"><xsl:value-of select="$ContainerDescription"/></fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Accum</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right"> 
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold"></fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell text-align="left" number-columns-spanned="4" border-bottom="{$BorderStyle}">
					<fo:block space-before="1mm" space-after="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold"/>
				</fo:table-cell>
				<fo:table-cell text-align="right" border-bottom="{$BorderStyle}">
					<fo:block space-before="1mm" space-after="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold"/>
				</fo:table-cell>
				<fo:table-cell text-align="right" border-bottom="{$BorderStyle}">
					<fo:block space-before="1mm" space-after="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Items</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right" border-bottom="{$BorderStyle}">
					<fo:block space-before="1mm" space-after="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold"/>
				</fo:table-cell>
				<fo:table-cell text-align="right" border-bottom="{$BorderStyle}">
					<fo:block space-before="1mm" space-after="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold"><xsl:value-of select="$ContainerDescription"/></fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right" border-bottom="{$BorderStyle}">
					<fo:block space-before="1mm" space-after="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold"></fo:block>
				</fo:table-cell>																				
			</fo:table-row>
		</fo:table-header>		
	</xsl:template>
	
	<xsl:template name="ListingAll">		
		<fo:table table-layout="fixed">
			<xsl:call-template name="ListingColumns"></xsl:call-template>
			<xsl:call-template name="ListingPageHeader"></xsl:call-template>
			<fo:table-body>
				<xsl:for-each select="EligibleSelections/Selection">
					<xsl:call-template name="ListingRow">
						<xsl:with-param name="spaceafter" select="'0mm'"/>
					</xsl:call-template>
				</xsl:for-each>
				<fo:table-row keep-with-previous="always">
					<fo:table-cell number-columns-spanned="4">
						<fo:block space-before="2mm"/>
					</fo:table-cell>
					<fo:table-cell border-top="{$BorderStyle}">
						<fo:block space-before="2mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number($mailsortitems,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="{$BorderStyle}">
						<fo:block space-before="2mm"/>
					</fo:table-cell>
					<fo:table-cell border-top="{$BorderStyle}">
						<fo:block space-before="2mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number($directbags + $residuebags,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="{$BorderStyle}">
						<fo:block space-before="2mm"/>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>															
	</xsl:template>
	
	<xsl:template name="ListingTotal">
		<fo:table-row>
			<fo:table-cell text-align="left" number-columns-spanned="4">
				<fo:block space-before="3mm" font-family="{$h5-family}" font-size="{$h5-size}"/>
			</fo:table-cell>
			<fo:table-cell text-align="right">
				<fo:block space-before="3mm" font-family="{$h5-family}" font-size="{$h5-size}"/>
			</fo:table-cell>
			<fo:table-cell text-align="right">
				<fo:block space-before="3mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">
					<xsl:value-of select="format-number($mailsortitems,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell text-align="right">
				<fo:block space-before="3mm" font-family="{$h5-family}" font-size="{$h5-size}"/>
			</fo:table-cell>
			<fo:table-cell text-align="right">
				<fo:block space-before="3mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">
					<xsl:value-of select="format-number($residuebags + $directbags,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>		
	</xsl:template>
	
	<xsl:template name="ListingDirect">
		<xsl:param name="Totals"></xsl:param>		
		<fo:table table-layout="fixed">
			<xsl:call-template name="ListingColumns"></xsl:call-template>
			<xsl:call-template name="ListingPageHeader"></xsl:call-template>
			<fo:table-body>
				<xsl:for-each select="EligibleSelections/Selection">
					<xsl:if test="not(substring(Code,4,2)='00') and string-length(Code) > 3">			         <!-- Test for = Direct Selection -->
						<xsl:call-template name="ListingRow">
							<xsl:with-param name="spaceafter" select="'0mm'"/>
							<xsl:with-param name="residue" select="substring(Code,4,2)='00' or string-length(Code) = 3"/>
						</xsl:call-template>						
					</xsl:if>
				</xsl:for-each>
				<xsl:if test="$Totals = 'true'">
					<xsl:call-template name="ListingTotal"/>
				</xsl:if>				
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<xsl:template name="ListingResidue">
		<xsl:param name="Totals"></xsl:param>		
		<fo:table table-layout="fixed">
			<xsl:call-template name="ListingColumns"></xsl:call-template>
			<xsl:call-template name="ListingPageHeader"></xsl:call-template>
			<fo:table-body>
				<xsl:for-each select="EligibleSelections/Selection">
					<xsl:if test="substring(Code,4,2)='00' or string-length(Code) = 3">			         		<!-- Test for = Residue Selection -->
						<xsl:call-template name="ListingRow">
							<xsl:with-param name="spaceafter" select="'0mm'"/>
							<xsl:with-param name="residue" select="substring(Code,4,2)='00' or string-length(Code) = 3"/>
						</xsl:call-template>						
					</xsl:if>
				</xsl:for-each>
				<xsl:if test="$Totals = 'true'">
					<xsl:call-template name="ListingTotal"/>
				</xsl:if>				
			</fo:table-body>
		</fo:table>
	</xsl:template>
	
	<!-- ++++++++++++++++++++++++++++++++++                Elibigle Selection Listings - Mixed Weight              ++++++++++++++++++++++++++++  -->
	
	<xsl:template name="ListingMixedWeightAlternate">
		<!-- Output all selections in the order provided by the sort engine -->		
		<fo:page-sequence master-reference="A4Landscape">
			<xsl:call-template name="Header">
				<xsl:with-param name="Title" select="'Eligible Selections'"/>
				<xsl:with-param name="USE-APIDemo" select="$USE-APIDemo"/>				
			</xsl:call-template>
			<xsl:call-template name="Footer"/>
			<fo:flow flow-name="xsl-region-body" font-family="{$d1-family}" font-size="{$d1-size}" font-weight="normal">
				<xsl:call-template name="ListingMixedWeightAll"/>
			</fo:flow>
		</fo:page-sequence>
	</xsl:template>
		
	<xsl:template name="ListingMixedWeightLowSort">
		<!-- LowSort is same logic as Interleaved ... just a single listing of Eligible Selections -->
		<xsl:call-template name="ListingMixedWeightAlternate"></xsl:call-template>
	</xsl:template>
	
	<xsl:template name="ListingMixedWeightResidueDirect">
		<!-- First call Residue Selections -->		
		<fo:page-sequence master-reference="A4Landscape">
			<xsl:call-template name="Header">
				<xsl:with-param name="Title" select="'Residue Listing'"/>
				<xsl:with-param name="USE-APIDemo" select="$USE-APIDemo"/>				
			</xsl:call-template>
			<xsl:call-template name="Footer"/>
			<fo:flow flow-name="xsl-region-body" font-family="{$d1-family}" font-size="{$d1-size}" font-weight="normal">
				<xsl:call-template name="ListingMixedWeightResidue"/>
			</fo:flow>
		</fo:page-sequence>
		
		<!-- Next call Direct Selections -->		
		<fo:page-sequence master-reference="A4Landscape">
			<xsl:call-template name="Header">
				<xsl:with-param name="Title" select="'Direct Listing'"/>
				<xsl:with-param name="USE-APIDemo" select="$USE-APIDemo"/>				
			</xsl:call-template>
			<xsl:call-template name="Footer"/>
			<fo:flow flow-name="xsl-region-body" font-family="{$d1-family}" font-size="{$d1-size}" font-weight="normal">
				<xsl:call-template name="ListingMixedWeightDirect"/>
			</fo:flow>
		</fo:page-sequence>
	</xsl:template>
	
	<xsl:template name="ListingMixedWeightDirectResidue">
		<!-- First call Direct Selections -->		
		<fo:page-sequence master-reference="A4Landscape">
			<xsl:call-template name="Header">
				<xsl:with-param name="Title" select="'Direct Listing'"/>
				<xsl:with-param name="USE-APIDemo" select="$USE-APIDemo"/>				
			</xsl:call-template>
			<xsl:call-template name="Footer"/>
			<fo:flow flow-name="xsl-region-body" font-family="{$d1-family}" font-size="{$d1-size}" font-weight="normal">
				<xsl:call-template name="ListingMixedWeightDirect"/>
			</fo:flow>
		</fo:page-sequence>
		
		<!-- Next call Residue Selections -->		
		<fo:page-sequence master-reference="A4Landscape">
			<xsl:call-template name="Header">
				<xsl:with-param name="Title" select="'Residue Listing'"/>
				<xsl:with-param name="USE-APIDemo" select="$USE-APIDemo"/>				
			</xsl:call-template>
			<xsl:call-template name="Footer"/>
			<fo:flow flow-name="xsl-region-body" font-family="{$d1-family}" font-size="{$d1-size}" font-weight="normal">
				<xsl:call-template name="ListingMixedWeightResidue"/>
			</fo:flow>
		</fo:page-sequence>		
	</xsl:template>
		
	<xsl:template name="ListingMixedWeightRow">
		<xsl:param name="spaceafter"/>
		<xsl:param name="residue"/>
		<fo:table-row>
			<fo:table-cell padding-left="2mm">
				<fo:block space-after="{$spaceafter}" space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="normal">
					<xsl:choose>
						<xsl:when test="$residue or $lowsort">
							<xsl:value-of select="substring(Code,1,3)"/>			<!-- For residue we don't print the 00 at end of selection code -->		
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="Code"/>									
						</xsl:otherwise>
					</xsl:choose>					
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-after="{$spaceafter}" space-before="1mm" text-align="right">
					<xsl:value-of select="format-number(Items,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-after="{$spaceafter}" space-before="1mm" text-align="right">
					<xsl:value-of select="format-number(AccumItems,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-right="4mm">
				<fo:block space-after="{$spaceafter}" space-before="1mm" text-align="right">
					<xsl:value-of select="format-number(Bags,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-after="{$spaceafter}" space-before="1mm" text-align="right">
					<xsl:value-of select="format-number(Band1Items,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-after="{$spaceafter}" space-before="1mm" text-align="right">
					<xsl:value-of select="format-number(Band1AcctWeight,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-right="4mm">
				<fo:block space-after="{$spaceafter}" space-before="1mm" text-align="right">
					<xsl:value-of select="format-number(Band1AssuWeight,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-after="{$spaceafter}" space-before="1mm" text-align="right">
					<xsl:value-of select="format-number(Band2Items,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-after="{$spaceafter}" space-before="1mm" text-align="right">
					<xsl:value-of select="format-number(Band2AcctWeight,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-right="4mm">
				<fo:block space-after="{$spaceafter}" space-before="1mm" text-align="right">
					<xsl:value-of select="format-number(Band2AssuWeight,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-after="{$spaceafter}" space-before="1mm" text-align="right">
					<xsl:value-of select="format-number(Band3Items,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding-right="4mm">
				<fo:block space-after="{$spaceafter}" space-before="1mm" text-align="right">
					<xsl:value-of select="format-number(Band3AcctWeight,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-after="{$spaceafter}" space-before="1mm" text-align="right">
					<xsl:value-of select="format-number(TotalAcctWeight,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-after="{$spaceafter}" space-before="1mm" text-align="right">
					<xsl:value-of select="format-number(TotalAssuWeight,'###,###,##0')"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-after="{$spaceafter}" space-before="1mm" text-align="left" color="red">
					<xsl:value-of select="Islands"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
				
	<xsl:template name="ListingMixedWeightColumns">
		<fo:table-column column-width="15mm"/><!-- ssc -->
		<fo:table-column column-width="13.2mm"/><!-- items/total -->
		<fo:table-column column-width="18.5mm"/><!-- items/accum -->
		<fo:table-column column-width="16mm"/><!-- bags/total -->
		<fo:table-column column-width="13.2mm"/><!-- lowband/items -->
		<fo:table-column column-width="24mm"/><!-- lowband/actual weight -->
		<fo:table-column column-width="24mm"/><!-- lowband/assumed weight -->
		<fo:table-column column-width="13.2mm"/><!-- middleband/items -->
		<fo:table-column column-width="24mm"/><!-- middleband/actual weight -->
		<fo:table-column column-width="24mm"/><!-- middleband/assumed weight -->
		<fo:table-column column-width="13.2mm"/><!-- highband/items -->
		<fo:table-column column-width="25mm"/><!-- highband/weight -->
		<fo:table-column column-width="24mm"/><!-- totalweights/actual -->
		<fo:table-column column-width="24mm"/><!-- totalweights/assumed -->
	</xsl:template>

	<xsl:template name="ListingMixedWeightPageHeader">
		<xsl:if test="not($dsa)">							
			<fo:table-row>
				<fo:table-cell text-align="left">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold"/>
				</fo:table-cell>
				<fo:table-cell text-align="left" number-columns-spanned="2">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">|----- Total Items ----|</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="center" padding-right="4mm">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold"/>
				</fo:table-cell>
				<fo:table-cell text-align="left" number-columns-spanned="3" padding-right="4mm">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">|---------------- Up to 100g ----------------|</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="left" number-columns-spanned="3" padding-right="4mm">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">|---------------- 101g - 250g ---------------|</fo:block>
				</fo:table-cell>
				<xsl:choose>
					<xsl:when test="contains($format, 'Parcel') ">																																																													
						<fo:table-cell text-align="left" number-columns-spanned="2" padding-right="4mm">
							<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">|---- 251g - 2000g ----|</fo:block>
						</fo:table-cell>
					</xsl:when>
					<xsl:otherwise>
						<fo:table-cell text-align="left" number-columns-spanned="2" padding-right="4mm">
							<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">|------ 251g - 750g -----|</fo:block>
						</fo:table-cell>
					</xsl:otherwise>
				</xsl:choose>
				<fo:table-cell text-align="left" number-columns-spanned="2">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">|---------- Total Weight -----------|</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell text-align="left">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Selections</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Items</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Accum</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right" padding-right="4mm">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold"><xsl:value-of select="$ContainerDescription"/></fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Items</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Act. Wgt</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right" padding-right="4mm">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Ass. Wgt</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Items</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Act. Wgt</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right" padding-right="4mm">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Ass. Wgt</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Items</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right" padding-right="4mm">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Act/Ass. Wgt</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Actual</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Assumed</fo:block>
				</fo:table-cell>
			</fo:table-row>				
		</xsl:if>
				
		<xsl:if test="$tnt = 'true'">							
			<fo:table-row>
				<fo:table-cell text-align="left">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold"/>
				</fo:table-cell>
				<fo:table-cell text-align="left" number-columns-spanned="2">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">|----- Total Items ----|</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="center" padding-right="4mm">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold"/>
				</fo:table-cell>
												
				<xsl:choose>
					<xsl:when test="contains($format, 'Parcel') ">
						<fo:table-cell text-align="left" number-columns-spanned="3" padding-right="4mm">
							<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">|---------------- 1000g ----------------|</fo:block>
						</fo:table-cell>
					</xsl:when>
					<xsl:otherwise>
						<fo:table-cell text-align="left" number-columns-spanned="3" padding-right="4mm">
							<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">|------------ Up to 100g -----------|</fo:block>
						</fo:table-cell>
					</xsl:otherwise>
				</xsl:choose>
												
				<xsl:choose>
					<xsl:when test="contains($format, 'Parcel') ">
						<fo:table-cell text-align="left" number-columns-spanned="3" padding-right="4mm">
							<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">|----------- 1001g - 2000g -----------|</fo:block>
						</fo:table-cell>
					</xsl:when>
					<xsl:otherwise>
						<fo:table-cell text-align="left" number-columns-spanned="3" padding-right="4mm">
							<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">|------------- 101g - 250g -----------|</fo:block>
						</fo:table-cell>
					</xsl:otherwise>
				</xsl:choose>
												
				<xsl:choose>
					<xsl:when test="contains($format, 'Parcel') ">
						<fo:table-cell text-align="left" number-columns-spanned="2" padding-right="4mm">
							<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">|----------- n/a -----------|</fo:block>
						</fo:table-cell>
					</xsl:when>
					<xsl:otherwise>
						<fo:table-cell text-align="left" number-columns-spanned="2" padding-right="4mm">
							<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">|------ 251g - 750g ------|</fo:block>
						</fo:table-cell>
					</xsl:otherwise>
				</xsl:choose>
				<fo:table-cell text-align="left" number-columns-spanned="2">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">|-------- Total Weight --------|</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell text-align="left">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Selections</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Items</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Accum</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right" padding-right="4mm">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold"><xsl:value-of select="standard/analysis/parameters/container"/></fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Items</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Act. Wgt</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right" padding-right="4mm">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Ass. Wgt</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Items</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Act. Wgt</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right" padding-right="4mm">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Ass. Wgt</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Items</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right" padding-right="4mm">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Act/Ass. Wgt</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Actual</fo:block>
				</fo:table-cell>
				<fo:table-cell text-align="right">
					<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Assumed</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</xsl:if>
	</xsl:template>

	<xsl:template name="ListingMixedWeightAll">
		<fo:table table-layout="fixed" width="100%">
			<xsl:call-template name="ListingMixedWeightColumns"/><!-- Defines the 14 columns -->
			<fo:table-header>
				<xsl:call-template name="ListingMixedWeightPageHeader"/><!-- Headers for the 14 columns -->
			</fo:table-header>
			<fo:table-body>
				<xsl:for-each select="EligibleSelections/Selection">
					<xsl:call-template name="ListingMixedWeightRow">
						<xsl:with-param name="spaceafter" select="'0mm'"/>
					</xsl:call-template>
				</xsl:for-each>
				<fo:table-row keep-with-previous="always"><!-- The totals row at the end of the report -->
					<fo:table-cell>
						<fo:block space-before="1mm"/>
					</fo:table-cell>
					<fo:table-cell border-top="{$BorderStyle}">
						<fo:block space-before="3mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number(sum(/Report/EligibleSelections/Selection/Items),'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="{$BorderStyle}">
						<fo:block space-before="3mm" text-align="right" font-weight="bold"/>
					</fo:table-cell>
					<fo:table-cell border-top="{$BorderStyle}" padding-right="4mm">
						<fo:block space-before="3mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number(sum(/Report/EligibleSelections/Selection/Bags),'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="{$BorderStyle}">
						<fo:block space-before="3mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number(sum(/Report/EligibleSelections/Selection/Band1Items),'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="{$BorderStyle}">
						<fo:block space-before="3mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number(sum(/Report/EligibleSelections/Selection/Band1AcctWeight),'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="{$BorderStyle}" padding-right="4mm">
						<fo:block space-before="3mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number(sum(/Report/EligibleSelections/Selection/Band1AssuWeight),'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="{$BorderStyle}">
						<fo:block space-before="3mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number(sum(/Report/EligibleSelections/Selection/Band2Items),'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="{$BorderStyle}">
						<fo:block space-before="3mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number(sum(/Report/EligibleSelections/Selection/Band2AcctWeight),'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="{$BorderStyle}" padding-right="4mm">
						<fo:block space-before="3mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number(sum(/Report/EligibleSelections/Selection/Band2AssuWeight),'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="{$BorderStyle}">
						<fo:block space-before="3mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number(sum(/Report/EligibleSelections/Selection/Band3Items),'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="{$BorderStyle}" padding-right="4mm">
						<fo:block space-before="3mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number(sum(/Report/EligibleSelections/Selection/Band3AcctWeight),'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="{$BorderStyle}">
						<fo:block space-before="3mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number(sum(/Report/EligibleSelections/Selection/TotalAcctWeight),'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell border-top="{$BorderStyle}">
						<fo:block space-before="3mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number(sum(/Report/EligibleSelections/Selection/TotalAssuWeight),'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>

	<xsl:template name="ListingMixedWeightDirect">
		<fo:table table-layout="fixed" width="100%">
			<xsl:call-template name="ListingMixedWeightColumns"/><!-- Defines the 14 columns -->
			<fo:table-header>
				<xsl:call-template name="ListingMixedWeightPageHeader"/><!-- Headers for the 14 columns -->
			</fo:table-header>
			<fo:table-body>
				<xsl:for-each select="EligibleSelections/Selection">
					<xsl:if test="not(substring(Code,4,2)='00')">			         <!-- Test for = Direct Selection -->
						<xsl:call-template name="ListingMixedWeightRow">
							<xsl:with-param name="spaceafter" select="'0mm'"/>
							<xsl:with-param name="residue" select="substring(Code,4,2)='00'"/>
						</xsl:call-template>						
					</xsl:if>
				</xsl:for-each>
			</fo:table-body>
		</fo:table>
	</xsl:template>

	<xsl:template name="ListingMixedWeightResidue">
		<fo:table table-layout="fixed" width="100%">
			<xsl:call-template name="ListingMixedWeightColumns"/><!-- Defines the 14 columns -->
			<fo:table-header>
				<xsl:call-template name="ListingMixedWeightPageHeader"/><!-- Headers for the 14 columns -->
			</fo:table-header>
			<fo:table-body>
				<xsl:for-each select="EligibleSelections/Selection">
					<xsl:if test="substring(Code,4,2)='00'">				    	<!-- Test for = Residue Selection -->
						<xsl:call-template name="ListingMixedWeightRow">
							<xsl:with-param name="spaceafter" select="'0mm'"/>
							<xsl:with-param name="residue" select="substring(Code,4,2)='00'"/>
						</xsl:call-template>						
					</xsl:if>
				</xsl:for-each>
			</fo:table-body>
		</fo:table>
	</xsl:template>

	<!-- ++++++++++++++++++++++++++++++++++                       Items Posted in Standard Post                    ++++++++++++++++++++++++++++  -->	
	<xsl:template name="StandardEntry">
		<xsl:param name="selection"/>
		<xsl:param name="description"/>		
		<xsl:param name="items"/>		
		<fo:table-row>
			<fo:table-cell>
				<fo:block space-before="1mm"/>
			</fo:table-cell><!-- for centering -->
			<fo:table-cell>
				<fo:block space-before="1mm" text-align="left">
					<xsl:value-of select="$selection"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-before="1mm" text-align="left">
					<xsl:value-of select="$description"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-before="1mm" text-align="right">
					<xsl:value-of select="$items"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block space-before="1mm"/>
			</fo:table-cell><!-- for centering -->
		</fo:table-row>		
	</xsl:template>
	
	<xsl:template name="StandardRows">
		<fo:table table-layout="fixed" width="100%">
			<fo:table-column column-width="proportional-column-width(1)"/><!-- for centering -->
			<fo:table-column column-width="26mm"/>
			<fo:table-column column-width="54mm"/>
			<fo:table-column column-width="20mm"/>
			<fo:table-column column-width="proportional-column-width(1)"/><!-- for centering -->
			<fo:table-header>
				<fo:table-row>
					<fo:table-cell>
						<fo:block space-before="1mm"/>
					</fo:table-cell><!-- for centering -->
					<fo:table-cell text-align="left" border-bottom="{$BorderStyle}">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Selection</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right" border-bottom="{$BorderStyle}">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}"/>
					</fo:table-cell>
					<fo:table-cell text-align="right" border-bottom="{$BorderStyle}">
						<fo:block space-before="1mm" font-family="{$h5-family}" font-size="{$h5-size}" font-weight="bold">Items</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="1mm"/>
					</fo:table-cell><!-- for centering -->
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<xsl:call-template name="StandardEntry">
					<xsl:with-param name="selection" select="'UK'"/>
					<xsl:with-param name="description" select="'REJECTS'"/>
					<xsl:with-param name="items" select="$UKUnmatchedItems"/>
				</xsl:call-template>
				<xsl:call-template name="StandardEntry">
					<xsl:with-param name="selection" select="'UK'"/>
					<xsl:with-param name="description" select="'WITHDRAWN'"/>
					<xsl:with-param name="items" select="$UKWithdrawnItems"/>
				</xsl:call-template>
				<xsl:call-template name="StandardEntry">
					<xsl:with-param name="selection" select="'EUROPE'"/>
					<xsl:with-param name="description" select="''"/>
					<xsl:with-param name="items" select="$EuropeItems"/>
				</xsl:call-template>
				<xsl:call-template name="StandardEntry">
					<xsl:with-param name="selection" select="'ROW'"/>
					<xsl:with-param name="description" select="'Zone1'"/>
					<xsl:with-param name="items" select="$Zone1Items"/>
				</xsl:call-template>
				<xsl:call-template name="StandardEntry">
					<xsl:with-param name="selection" select="'ROW'"/>
					<xsl:with-param name="description" select="'Zone2'"/>
					<xsl:with-param name="items" select="$Zone2Items"/>
				</xsl:call-template>
								
				<fo:table-row keep-with-previous="always">
					<fo:table-cell>
						<fo:block space-before="1mm"/>
					</fo:table-cell><!-- for centering -->
					<fo:table-cell>
						<fo:block space-before="3mm" text-align="left"/>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="3mm" text-align="left"/>
					</fo:table-cell>
					<fo:table-cell border-top="{$BorderStyle}">
						<fo:block space-before="3mm" text-align="right" font-weight="bold">
							<xsl:value-of select="format-number($TotalStandardTariffItems,'###,###,##0')"/>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block space-before="1mm"/>
					</fo:table-cell><!-- for centering -->
				</fo:table-row>
			</fo:table-body>
		</fo:table>		
	</xsl:template>
	
	<xsl:template name="UnsortedPlanningPageController">
		<xsl:call-template name="Header">
			<xsl:with-param name="Title" select= "$PlanningAnalysis"/> 
			<xsl:with-param name="USE-APIDemo" select="$USE-APIDemo"/>			
		</xsl:call-template>
		<xsl:call-template name="Footer"/>
		<fo:flow flow-name="xsl-region-body" font-family="{$d1-family}" font-size="{$d1-size}" font-weight="normal">
			<xsl:if test="not($SortOK)">
				<xsl:call-template name="PlanningInvalid"/>								<!-- Template works for both Sorted and UnSorted -->
			</xsl:if>
			<xsl:call-template name="PlanningParameters"/>								<!-- Template works for both Sorted and UnSorted -->
			
			<fo:block space-before="5mm" space-after="1mm" font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Sortation Statistics</fo:block>
			
			<fo:table table-layout="fixed" width="8cm">									<!-- Counts for UK, BFPO, International etc unique to UnSorted -->
				<fo:table-column column-width="3cm"/>
				<fo:table-column column-width="5cm"/>					
				<fo:table-body>
					<xsl:if test="not($mailmark)">
						<xsl:call-template name="QuantityRow">
							<xsl:with-param name="Title" select="'UK'"/>
							<xsl:with-param name="Value" select="$uk"/>
						</xsl:call-template>									
						<xsl:call-template name="QuantityRow">
							<xsl:with-param name="Title" select="'BFPO'"/>
							<xsl:with-param name="Value" select="$bfpo"/>
						</xsl:call-template>									
						<xsl:call-template name="QuantityRow">
							<xsl:with-param name="Title" select="'International'"/>
							<xsl:with-param name="Value" select="$international"/>
						</xsl:call-template>															
					</xsl:if>
					<xsl:call-template name="QuantityRowBold">
						<xsl:with-param name="Title" select="'Eligible'"/>
						<xsl:with-param name="Value" select="$eligible"/>
					</xsl:call-template>									
					<xsl:call-template name="QuantityRow">
						<xsl:with-param name="Title" select="'Ineligible'"/>
						<xsl:with-param name="Value" select="$ineligible"/>
					</xsl:call-template>									
					<xsl:call-template name="QuantityRowBold">
						<xsl:with-param name="Title" select="'Total'"/>
						<xsl:with-param name="Value" select="$total"/>
					</xsl:call-template>									
				</fo:table-body>
			</fo:table>
			
			<xsl:if test="$mailmark">
				<xsl:call-template name="ListingParametersMailmark"></xsl:call-template>
			</xsl:if>
			
			<xsl:if test="$tnt">
				<xsl:call-template name="PlanningWhistl"/>				
			</xsl:if>
			
			<xsl:if test="$OutputFiles">
				<xsl:call-template name="PlanningOutputFiles"></xsl:call-template>
			</xsl:if>
		</fo:flow>
	</xsl:template>
	
	<xsl:template name="UnSortedListingController">
		<xsl:call-template name="Header">
			<xsl:with-param name="Title" select='"Container Breakdown"'/> 
			<xsl:with-param name="USE-APIDemo" select="$USE-APIDemo"/>			
		</xsl:call-template>		
		<xsl:call-template name="Footer"/>
		<fo:flow flow-name="xsl-region-body" font-family="{$d1-family}" font-size="{$d1-size}" font-weight="normal">
			<xsl:call-template name="UnsortedListing">
				<xsl:with-param name="nodes" select="BagBreakdown/NationalSelections" />	
				<xsl:with-param name="tableheader" select="'National Mail (UK) Items'"></xsl:with-param>
			</xsl:call-template>
			
			<xsl:if test="BagBreakdown/BFPOSelections">
				<xsl:call-template name="UnsortedListing">
					<xsl:with-param name="nodes" select="BagBreakdown/BFPOSelections" />				
					<xsl:with-param name="tableheader" select="'BFPO Items'"></xsl:with-param>
				</xsl:call-template>				
			</xsl:if>
			
			<xsl:if test="BagBreakdown/InternationalSelections">
				<xsl:call-template name="UnsortedListing">
					<xsl:with-param name="nodes" select="BagBreakdown/InternationalSelections" />				
					<xsl:with-param name="tableheader" select="'International Items'" />
				</xsl:call-template>				
			</xsl:if>
			
		</fo:flow>
	</xsl:template>
	
	<xsl:template name="UnsortedListing">
		<xsl:param name="nodes"/>
		<xsl:param name="tableheader"/>
		<fo:table table-layout="fixed" width="8cm">									<!-- Counts for UK, BFPO, International etc unique to UnSorted -->
			<fo:table-column column-width="3cm"/>
			<fo:table-column column-width="5cm"/>
			<fo:table-column column-width="4cm"/>
			<fo:table-column column-width="3cm"/>
			<fo:table-column column-width="3cm"/>
			<fo:table-header>
				<xsl:if test="not($mailmark)">
					<fo:table-row keep-with-next="always">
						<fo:table-cell number-columns-spanned="5">
							<fo:block space-before="5mm" space-after="5mm" font-family="{$h3-family}" font-size="{$h3-size}" font-weight="bold">
								<xsl:value-of select="$tableheader"/>
							</fo:block>						
						</fo:table-cell>
					</fo:table-row>					
				</xsl:if>
				<fo:table-row keep-with-next="always">
					<fo:table-cell text-align="right">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Container</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="left" padding-left="5mm">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Container ID</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Container Start</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Container End</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="right">
						<fo:block font-family="{$h4-family}" font-size="{$h4-size}" font-weight="bold">Items</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>				
			<fo:table-body>
				<xsl:for-each select="$nodes/Selection">
					<fo:table-row>
						<fo:table-cell text-align="right">
							<fo:block>
								<xsl:value-of select="format-number(Number,'###,###,##0')"/>
							</fo:block>	
						</fo:table-cell>
						<fo:table-cell padding-left="5mm">
							<fo:block>
								<xsl:value-of select="Id"/>
							</fo:block>								
						</fo:table-cell>
						<fo:table-cell text-align="right">
							<fo:block>
								<xsl:value-of select="Start"/>
							</fo:block>								
						</fo:table-cell>
						<fo:table-cell text-align="right">
							<fo:block>
								<xsl:value-of select="End"/>
							</fo:block>								
						</fo:table-cell>
						<fo:table-cell text-align="right">
							<fo:block>
								<xsl:value-of select="Items"/>
							</fo:block>								
						</fo:table-cell>
					</fo:table-row>
				</xsl:for-each>
			</fo:table-body>
		</fo:table>					
	</xsl:template>	
</xsl:stylesheet>

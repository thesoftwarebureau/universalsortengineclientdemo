<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <!-- NEED TO INJECT MYSELF -->
    <xsl:variable name="SortOK" select="/Report/SortOK='True'"/>
    <xsl:variable name="ErrorMessage" select="/Report/ErrorMessage"/>
      
    <!-- OUTPUT FILES **** THIS IS EXTRA TAGS THAT MY APPLICATION WILL NEED TO CREATE AND PASS INTO THE SORTATION **** -->
    <xsl:variable name="OutputFiles" select="/Report/OutputFiles"/>                                                
    <xsl:variable name="BagLabelFile" select="/Report/OutputFiles/BagLabel"/>                                                
    <xsl:variable name="SimpleBagLabelFile" select="/Report/OutputFiles/SimpleBagLabel"/>                                                
    <xsl:variable name="UploadFile" select="/Report/OutputFiles/Upload"/>                                                
    <xsl:variable name="ProductionFile" select="/Report/OutputFiles/Production"/>                                                
    <xsl:variable name="SectorsFile" select="/Report/OutputFiles/Sectors"/>                                                
    
    <!-- Output Files for OnePost -->
    <!-- <xsl:variable name="OnePostPlannyingAnalysis" select="/Report/OutputFiles/PlanningAnalysis"></xsl:variable> -->
    <xsl:variable name="Directories" select="/Report/OutputFiles/Directories/Directory"></xsl:variable>
    <xsl:variable name="OnePostMainFiles" select="/Report/OutputFiles/MainFiles/File"></xsl:variable>
    <xsl:variable name="OnePostBagFiles" select="/Report/OutputFiles/BagFiles/File"></xsl:variable>
    <xsl:variable name="OnePostOtherFiles" select="/Report/OutputFiles/OtherFiles/File"></xsl:variable>
    
    <!-- MAILMARK PARAMETERS -->
    <xsl:variable name="PlanningAnalysis">
      <xsl:value-of select="'Planning Analysis '"/>
      <xsl:value-of select="$MailmarkCarrierName"/>
    </xsl:variable>
    <xsl:variable name="MailmarkSupplyChainId" select="/Report/PlanningAnalysis/mailmarkparams/supplychainid"/>        
    <xsl:variable name="MailmarkOwnerName" select="/Report/PlanningAnalysis/mailmarkparams/ownername"/>        
    <xsl:variable name="MailmarkCarrierName" select="/Report/PlanningAnalysis/mailmarkparams/carriername"/>        
    <xsl:variable name="MailmarkPayerName" select="/Report/PlanningAnalysis/mailmarkparams/payername"/>        
    <xsl:variable name="MailmarkProducerName" select="/Report/PlanningAnalysis/mailmarkparams/producername"/>        
    <xsl:variable name="MailmarkReturnPostCode" select="/Report/PlanningAnalysis/mailmarkparams/returnpostcode"/>        
    <xsl:variable name="MailmarkCampaignName" select="/Report/PlanningAnalysis/mailmarkparams/campaignname"/>        
    <xsl:variable name="MailmarkDepartmentName" select="/Report/PlanningAnalysis/mailmarkparams/departmentname"/>        
    <xsl:variable name="MailmarkOriginatorName" select="/Report/PlanningAnalysis/mailmarkparams/originatorname"/>
    
    <xsl:variable name="MailmarkMailSubType1" select="/Report/PlanningAnalysis/mailmarkparams/mailsubtype1"/>
    <xsl:variable name="MailmarkMailSubType2" select="/Report/PlanningAnalysis/mailmarkparams/mailsubtype2"/>
    <xsl:variable name="MailmarkMailSubType3" select="/Report/PlanningAnalysis/mailmarkparams/mailsubtype3"/>
    <xsl:variable name="MailmarkMailSubType4" select="/Report/PlanningAnalysis/mailmarkparams/mailsubtype4"/>
    
    <xsl:variable name="MailmarkSpare1-JICOptOut" select="/Report/PlanningAnalysis/mailmarkparams/spare1"/>        
    <xsl:variable name="MailmarkSpare2-MailInfoTypeID" select="/Report/PlanningAnalysis/mailmarkparams/spare2"/>        
    <xsl:variable name="MailmarkSpare3" select="/Report/PlanningAnalysis/mailmarkparams/spare3"/>        
    <xsl:variable name="MailmarkSpare4" select="/Report/PlanningAnalysis/mailmarkparams/spare4"/>        
    <xsl:variable name="MailmarkSpare5" select="/Report/PlanningAnalysis/mailmarkparams/spare5"/>        
    <xsl:variable name="MailmarkSpare6" select="/Report/PlanningAnalysis/mailmarkparams/spare6"/>        
    <xsl:variable name="MailmarkSpare7" select="/Report/PlanningAnalysis/mailmarkparams/spare7"/>
    <xsl:variable name="MailmarkSpare8" select="/Report/PlanningAnalysis/mailmarkparams/spare8"/>        
    <!-- CustomerRef replaces the Spare9 field in the gemma file -->
    <xsl:variable name="CustomerRef" select="/Report/PlanningAnalysis/mailmarkparams/customer-ref"/>        
    <xsl:variable name="MailmarkSpare10" select="/Report/PlanningAnalysis/mailmarkparams/spare10"/>        
    
    <xsl:variable name="MailmarkAdvertisingAttribute1" select="/Report/PlanningAnalysis/mailmarkparams/advertisingattribute1"/>
    <xsl:variable name="MailmarkAdvertisingAttribute2" select="/Report/PlanningAnalysis/mailmarkparams/advertisingattribute2"/>
    <xsl:variable name="MailmarkAdvertisingAttribute3" select="/Report/PlanningAnalysis/mailmarkparams/advertisingattribute3"/>
    <xsl:variable name="MailmarkAdvertisingAttribute4" select="/Report/PlanningAnalysis/mailmarkparams/advertisingattribute4"/>
    
    <!-- DSA PARAMETERS -->
    <xsl:variable name="DSAAccountNumberAKAClient" select="/Report/DSAParameters/AccountNumber"/>
    <xsl:variable name="DSACollectionDate" select="/Report/DSAParameters/CollectionDate"/>
    <xsl:variable name="DSAZonal" select="/Report/DSAParameters/Zonal"/>
    <xsl:variable name="zonal" select="/process/standard/analysis/parameters/zonal"/>
    
    <xsl:variable name="WhistlMailingHouse" select="/Report/DSAParameters/WhistlMailingHouse"/>
    <xsl:variable name="WhistlSegmentCode" select="/Report/DSAParameters/WhistlSegmentCode"/>
    <xsl:variable name="WhistlPO" select="/Report/DSAParameters/WhistlPO"/>
    <xsl:variable name="WhistlService" select="/Report/DSAParameters/WhistlService"/>
    <xsl:variable name="WhistlDepot" select="/Report/DSAParameters/WhistlDepot"/>

    <!-- UNSORTED PARAMETERS -->
    <xsl:variable name="UnSorted" select="/Report/PlanningAnalysis/SortParameters/Unsorted='TRUE'"/>
    <xsl:variable name="BagWeight" select="/Report/ListingCriteria/SortParameters/BagWeight"/>
    
    <!-- SORT PARAMETERS -->
    <xsl:variable name="CurrentMailsortTable" select="/Report/PlanningAnalysis/SortParameters/CurrentMailsortTable"/>
    <xsl:variable name="TableEffectiveDate" select="/Report/PlanningAnalysis/SortParameters/TableEffectiveDate"/>
    <xsl:variable name="TableEffectiveDateYear" select="substring($TableEffectiveDate,1,4)" />
    <xsl:variable name="TableEffectiveDateMonth" select="substring($TableEffectiveDate,5,2)" />
    <xsl:variable name="TableEffectiveDateDay" select="substring($TableEffectiveDate,7,2)" />

    <xsl:variable name="TableChangeReason" select="/Report/PlanningAnalysis/SortParameters/TableChangeReason"/>
    <xsl:variable name="mixedweight" select="/Report/WeightStatistics"/>
    <xsl:variable name="regionalsort" select="/Report/PlanningAnalysis/SortParameters/RegionalSort='True'"/>
 	<xsl:variable name="Sustainability" select="/Report/PlanningAnalysis/SortParameters/Sustainability"/>      
    <xsl:variable name="format" select="/Report/PlanningAnalysis/SortParameters/Format"/>        
    <xsl:variable name="ContainerDescription" select="/Report/PlanningAnalysis/SortParameters/ContainerDesc"/>
    <xsl:variable name="ContainerDescriptionSingular" select="substring($ContainerDescription,1,string-length($ContainerDescription)-1)"/>
    <xsl:variable name="ContainerDescriptionMaxItems">
    <xsl:value-of select="'Max Items Per '"/><xsl:value-of select="$ContainerDescriptionSingular"/>
    </xsl:variable>
    <xsl:variable name="mailsortservice" select="/Report/PlanningAnalysis/SortParameters/MailSortService"/>
    <xsl:variable name="ukmail" select="contains(/Report/PlanningAnalysis/SortParameters/MailSortService, 'UKMail')"/>
    <xsl:variable name="tnt" select="contains(/Report/PlanningAnalysis/SortParameters/MailSortService, 'Whistl')"/>
    <xsl:variable name="securedmail" select="contains(/Report/PlanningAnalysis/SortParameters/MailSortService, 'SecuredMail')"/>
    <xsl:variable name="citipost" select="contains(/Report/PlanningAnalysis/SortParameters/MailSortService, 'Citipost')"/>
    <xsl:variable name="onepost" select="contains(/Report/PlanningAnalysis/SortParameters/MailSortService, 'OnePost')"/>
    
    <!-- Would have thought Low sort would be identified by Low being in SortServiceType but JR says lots of sorts are Low so simplest to check not High (1400) -->
    <xsl:variable name="lowsort" select="not(contains(/Report/PlanningAnalysis/SortParameters/MailSortService, 'High'))"/>  

    <xsl:variable name="mailmark" select="contains(/Report/PlanningAnalysis/SortParameters/MailSortService, 'Mailmark')"/>  
    <xsl:variable name="dsa" select="$ukmail or $tnt or $securedmail or $citipost or $onepost"/>
    <xsl:variable name="MailingRegion" select="/Report/PlanningAnalysis/SortParameters/MailingRegion"/>
    <xsl:variable name="MailingReference" select="/Report/PlanningAnalysis/SortParameters/MailingReference"/>
    <xsl:variable name="MinimumSelection" select="/Report/PlanningAnalysis/SortParameters/MinimumSelection"/>
    <xsl:variable name="MailType" select="/Report/PlanningAnalysis/SortParameters/MailType"/>
    <xsl:variable name="ItemWeight" select="/Report/PlanningAnalysis/SortParameters/ItemWeight"/>
    <xsl:variable name="MaxItemsPerBag" select="/Report/PlanningAnalysis/SortParameters/MaxItemsPerBag"/>
    <xsl:variable name="ServiceLevel" select="/Report/PlanningAnalysis/SortParameters/SortServiceLevel"/>
    
    <!-- below will be one off SeparateDirectResidue, SeparateResidueDirect or the interleaved AlternateDirectResidue, AlternateResidueDirect -->
    <xsl:variable name="DirectResidueAlternateOrder" select="/Report/PlanningAnalysis/SortParameters/DirectResidueOrder"/>    
    
    <xsl:variable name="MailingRegionDescription">
        <xsl:choose>
            <xsl:when test="$MailingRegion = 'A'"><xsl:value-of select="'Northern Ireland'"/></xsl:when>
            <xsl:when test="$MailingRegion = 'B'"><xsl:value-of select="'Scotland'"/></xsl:when>
            <xsl:when test="$MailingRegion = 'C'"><xsl:value-of select="'Dumfries and Galloway'"/></xsl:when>
            <xsl:when test="$MailingRegion = 'D'"><xsl:value-of select="'Carlisle'"/></xsl:when>
            <xsl:when test="$MailingRegion = 'E'"><xsl:value-of select="'North East England'"/></xsl:when>
            <xsl:when test="$MailingRegion = 'F'"><xsl:value-of select="'North West England'"/></xsl:when>
            <xsl:when test="$MailingRegion = 'G'"><xsl:value-of select="'Yorkshire'"/></xsl:when>
            <xsl:when test="$MailingRegion = 'H'"><xsl:value-of select="'Nottingham'"/></xsl:when>
            <xsl:when test="$MailingRegion = 'I'"><xsl:value-of select="'Midlands'"/></xsl:when>
            <xsl:when test="$MailingRegion = 'J'"><xsl:value-of select="'East Anglia'"/></xsl:when>
            <xsl:when test="$MailingRegion = 'K'"><xsl:value-of select="'South East England'"/></xsl:when>
            <xsl:when test="$MailingRegion = 'L'"><xsl:value-of select="'Portsmouth'"/></xsl:when>
            <xsl:when test="$MailingRegion = 'M'"><xsl:value-of select="'South West England'"/></xsl:when>
            <xsl:when test="$MailingRegion = 'N'"><xsl:value-of select="'South Coast'"/></xsl:when>
            <xsl:when test="$MailingRegion = 'O'"><xsl:value-of select="'South Wales'"/></xsl:when>
            <xsl:when test="$MailingRegion = 'P'"><xsl:value-of select="'Devon and Cornwall'"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="'REGION UNKNOWN'"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <!-- COSTS -->
    <xsl:variable name="directrate" select="/Report/PlanningAnalysis/Costs/DirectRate"/>
    <xsl:variable name="residuerate" select="/Report/PlanningAnalysis/Costs/ResidueRate"/>
    <xsl:variable name="levypercent" select="/Report/PlanningAnalysis/Costs/Levy"/>                                        <!-- This is something like 0.2% -->
    <xsl:variable name="levy" select="substring($levypercent, 1, string-length($levypercent) - 1)"/>                       <!-- Strip off the percent -->
    <xsl:variable name="straightlineitemcost" select="/Report/PlanningAnalysis/Savings/StandardTariffRate"/>               <!-- NO DIRECT EQUIVALENT ... IS THIS CORRECT -->
    <xsl:variable name="residueocr" select="/Report/PlanningAnalysis/Costs/ResidueRate"/>                                  <!-- used to be residueocr but no longer exists -->
       
    <!-- UNSORTED PRESORTATION STASTICS -->
    <xsl:variable name="uk" select="/Report/PlanningAnalysis/PresortationStatistics/UK"/>
    <xsl:variable name="bfpo" select="/Report/PlanningAnalysis/PresortationStatistics/BFPO"/>
    <xsl:variable name="international" select="/Report/PlanningAnalysis/PresortationStatistics/International"/>
    <xsl:variable name="eligible" select="/Report/PlanningAnalysis/PresortationStatistics/EligibleTotal"/>
    <xsl:variable name="ineligible" select="/Report/PlanningAnalysis/PresortationStatistics/IneligibleForMailSort"/>
    <xsl:variable name="total" select="/Report/PlanningAnalysis/PresortationStatistics/Total"/>
    
    <!-- PRESORTATION STASTICS -->
    <xsl:variable name="postcodematch" select="/Report/PlanningAnalysis/PresortationStatistics/PostCodeMatch"/>
    <xsl:variable name="posttownmatch" select="/Report/PlanningAnalysis/PresortationStatistics/PostTownMatch"/>
    <xsl:variable name="mailsortitems" select="/Report/PlanningAnalysis/PresortationStatistics/PostCodeMatch + /Report/PlanningAnalysis/PresortationStatistics/PostTownMatch"/>
    <xsl:variable name="standardtariffitems" select="/Report/PlanningAnalysis/PresortationStatistics/IneligibleForMailSort"/>
    <xsl:variable name="totalitems" select="$mailsortitems + $standardtariffitems"/>
    
    <!-- SORTATION STATISTICS -->    
    <xsl:variable name="TotalQualify" select="/Report/PlanningAnalysis/SortationStatistics/DirectItems + /Report/PlanningAnalysis/SortationStatistics/ResidueItems"/>
    <xsl:variable name="resitems" select="/Report/PlanningAnalysis/SortationStatistics/ResidueItems"/>
    <xsl:variable name="diritems" select="/Report/PlanningAnalysis/SortationStatistics/DirectItems"/>
    <xsl:variable name="directselections" select="/Report/PlanningAnalysis/SortationStatistics/DirectSelections"/>
    <xsl:variable name="directbags" select="/Report/PlanningAnalysis/SortationStatistics/DirectBags"/>
    <xsl:variable name="residueselections" select="/Report/PlanningAnalysis/SortationStatistics/ResidueSelections"/>
    <xsl:variable name="residuebags" select="/Report/PlanningAnalysis/SortationStatistics/ResidueBags"/>
    
    <!-- POSTCODE STATISTICS -->    
    <xsl:variable name="fullypostcoded" select="/Report/PlanningAnalysis/PostcodeStatistics/FullyPostcoded"/> 
    <xsl:variable name="outboundonly" select="/Report/PlanningAnalysis/PostcodeStatistics/OutboundOnly"/> 
    <xsl:variable name="postcodetotal" select="/Report/PlanningAnalysis/PostcodeStatistics/PostcodeTotal"/>
    <xsl:variable name="nopostcode" select="/Report/PlanningAnalysis/PostcodeStatistics/NoPostcode"/>
    <xsl:variable name="DPSMatch" select="/Report/PlanningAnalysis/PostcodeStatistics/DPSMatch"/>
    <xsl:variable name="DPSDefaults" select="/Report/PlanningAnalysis/PostcodeStatistics/DPSDefaults"/>

    <xsl:variable name="DPSMatchLength" select="string-length($DPSMatch)"/>
    <xsl:variable name="DPSMatchItems" select="number(substring($DPSMatch,1,($DPSMatchLength - 1)))"/>               <!-- This is necessary because the DPSMatch contains % at end -->
    <xsl:variable name="DPSDefaultsLength" select="string-length($DPSDefaults)"/>
    <xsl:variable name="DPSDefaultsItems" select="number(substring($DPSDefaults,1,($DPSDefaultsLength - 1)))"/>      <!-- This is necessary because the DPSDefaults contains % at end -->
          
    <!-- CALCULATIONS -->
    <xsl:variable name="straightcost" select="$TotalQualify * /Report/PlanningAnalysis/Costs/DirectRate div 100"/>
    <xsl:variable name="dirdiscount" select="/Report/PlanningAnalysis/SortationStatistics/DirectItems * $directrate div 100"/>
    <xsl:variable name="resdiscount" select="/Report/PlanningAnalysis/SortationStatistics/ResidueItems * $residuerate div 100"/>
    <xsl:variable name="levycost" select="($dirdiscount + $resdiscount) * $levy div 100"/>

    <xsl:variable name="x9" select="$dirdiscount + $resdiscount"/>      <!--  <xsl:variable name="x9" select="$dirdiscount + $resdiscount + $levycost"/> -->
    <xsl:variable name="x9MinusLevy" select="$x9 - $levycost"/>         <!-- x9MinusLevy added by PAC 27/6/2012 -->
    <xsl:variable name="x9PlusLevy" select="$x9 + $levycost"/>          <!-- x9PlusLevy added by SF 06/07/2016 -->
    
    <!-- ><xsl:variable name="STCost" select="$totqual * /process/standard/analysis/standardtariff/ukstandardtariff div 100"/> -->
    
    <xsl:variable name="STRateVRD" select="/Report/PlanningAnalysis/Savings/StandardTariffRate div 100"/>    
    <xsl:variable name="STCostVRD" select="$TotalQualify * /Report/PlanningAnalysis/Savings/StandardTariffRate div 100"/>        <!-- STCostVRD added by Stu 18/7/2012 -->
    
    <xsl:variable name="x10a" select="($resitems * $straightlineitemcost div 100) * ($residueocr div 100)"/>
    <xsl:variable name="x11" select="$x9 - $x10a"/>
    <xsl:variable name="x10b" select="($diritems * ($straightlineitemcost div 100)) * (/process/standard/analysis/costs/earlypostingdirect div 100)"/>
    <xsl:variable name="x10c" select="($diritems * ($straightlineitemcost div 100)) * (/process/standard/analysis/costs/earlypostingresidue div 100)"/>
    <xsl:variable name="x12" select="$x9 - $x10b - $x10c"/>
    <xsl:variable name="x14a" select="$x9 * $levy div 100"/>
    <xsl:variable name="x14b" select="$x11 * $levy div 100"/>
    <xsl:variable name="x14c" select="$x12 * $levy div 100"/>
    <xsl:variable name="x13" select="$x9 - $x10a - $x10b - $x10c"/>
    <xsl:variable name="x14d" select="$x13 * $levy div 100"/>
    <xsl:variable name="x15a" select="$x9 + $x14a"/>
    <xsl:variable name="x15b" select="$x11 + $x14b"/>
    <xsl:variable name="x15c" select="$x12 + $x14c"/>
    <xsl:variable name="x15d" select="$x13 + $x14d"/>
    <xsl:variable name="x16" select="(/Report/PlanningAnalysis/Savings/StandardTariffRate * (/Report/PlanningAnalysis/PresortationStatistics/PostcodeMatch + /Report/PlanningAnalysis/PresortationStatistics/PostTownMatch)) div 100"/>

    <xsl:variable name="EuropeItems" select="/Report/PlanningAnalysis/StandardTariffCostAnalysis/EuropeItems"/>
    <xsl:variable name="Zone1Items" select="/Report/PlanningAnalysis/StandardTariffCostAnalysis/Zone1Items"/>
    <xsl:variable name="Zone2Items" select="/Report/PlanningAnalysis/StandardTariffCostAnalysis/Zone2Items"/>
    <xsl:variable name="UKUnmatchedItems" select="/Report/PlanningAnalysis/StandardTariffCostAnalysis/UKUnmatchedItems"/>
    <xsl:variable name="UKWithdrawnItems" select="/Report/PlanningAnalysis/StandardTariffCostAnalysis/UKWithdrawnItems"/>
    <xsl:variable name="EuropeRate" select="/Report/PlanningAnalysis/StandardTariffCostAnalysis/EuropeRate"/>
    <xsl:variable name="Zone1Rate" select="/Report/PlanningAnalysis/StandardTariffCostAnalysis/Zone1Rate"/>
    <xsl:variable name="Zone2Rate" select="/Report/PlanningAnalysis/StandardTariffCostAnalysis/Zone2Rate"/>
    <xsl:variable name="UKUnmatchedRate" select="/Report/PlanningAnalysis/StandardTariffCostAnalysis/UKUnmatchedRate"/>
    <xsl:variable name="UKWithdrawnRate" select="/Report/PlanningAnalysis/StandardTariffCostAnalysis/UKWithdrawnRate"/>
    <xsl:variable name="EuropeCost" select="$EuropeItems * $EuropeRate div 100"/>
    <xsl:variable name="Zone1Cost" select="$Zone1Items * $Zone1Rate div 100"/>
    <xsl:variable name="Zone2Cost" select="$Zone2Items * $Zone2Rate div 100"/>
    <xsl:variable name="UKUnmatchedCost" select="$UKUnmatchedItems * $UKUnmatchedRate div 100"/>
    <xsl:variable name="UKWithdrawnCost" select="$UKWithdrawnItems * $UKWithdrawnRate div 100"/>
    <xsl:variable name="TotalStandardTariffItems" select="/Report/ListingCriteria/Volumes/StandardTariffItems"/>
</xsl:stylesheet>

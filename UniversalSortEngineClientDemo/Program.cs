﻿using SoftwareBureau.UniversalSortEngine;
using SoftwareBureau.USEAPIClient;
using SoftwareBureau.USEAPIFileClient;

Test935Citipost();

/// <summary>
/// Gets USEFileClient setting the directory for db and XSL to be below the CurrentDirectory
/// </summary>
/// <returns></returns>
USEFileClient UseFileClient(string currentDirectory)
{
    USEFileClient.ControlDirectory = $"..\\..\\..\\Control";
    var sort = new USEFileClient
    {
        MailSortBinFile = $"{currentDirectory}\\db\\RMSort.bin",
        XslPath = $"{currentDirectory}\\XSL\\usesort-fo.xsl",
    };
    return sort;
}

/// <summary>
/// Used as callback to get status messages from the USEFileClient object
/// </summary>
/// <param name="sender"></param>
/// <param name="message">Message to be displayed</param>
void DisplayStatusMessage(object sender, string message)
{
    Console.WriteLine(message);
}

void Test935Citipost()
{
    DisplayStatusMessage(new object(), "Starting sort 935Citipost");

    string InputFile = @"..\..\..\Input\Input.psv";

    string OutputDirectory = @"..\..\..\Output\";

    var currentDirectory = Directory.GetCurrentDirectory();

    DisplayStatusMessage(new object(), $"Directory.GetCurrentDirectory() returns {currentDirectory}");

    var fullOutputDirectory = Path.GetFullPath(OutputDirectory);
    Console.WriteLine($"Full OutputData path {fullOutputDirectory} extrapolated from {OutputDirectory}");

    var testDirectory = Path.Combine(fullOutputDirectory, "935Citipost");
    Console.WriteLine($"Test Directory {testDirectory}");

    var sortDefinition = new SortDefinition
    {
        Description = "935Citipost",
        Carrier = USEParams.Carriers.Citipost,
        Sorted = true,
        MailMark = false,
        InputFile = InputFile,
        OutputDirectory = testDirectory,
        OutputName = "Output",
        OutputExtension = ".txt",
        Options = new SortParam[]
        {
                    new SortParam<string>(SortParam.DsaSplitType.PropertyName, $"{USEDSAParams.DSASplitTypes.Arbitrage}"),
                    new SortParam<string>(SortParam.Piece.PropertyName, $"{USEParams.Pieces.LargeLetter}"),
                    new SortParam<string>(SortParam.ServiceLevel.PropertyName, $"{USEParams.ServiceLevels.Second}"),
                    new SortParam<string>(SortParam.MailingRef.PropertyName, "#935"),
                    new SortParam<string>(SortParam.Container.PropertyName, $"{USEParams.Containers.Trays}"),
                    new SortParam<string>(SortParam.SortType.PropertyName, $"{USEParams.SortTypes.High}"),
                    new SortParam<string>(SortParam.MailType.PropertyName, $"{USEParams.MailTypes.Business}"),
                    new SortParam<int>(SortParam.Weight.PropertyName, 55),
                    new SortParam<int>(SortParam.MaxContainerContentWeight.PropertyName, 7140),
                    new SortParam<int>(SortParam.MinimumSelectionSize.PropertyName, 25),
                    new SortParam<string>(SortParam.Sustain.PropertyName, $"{USEParams.Sustainabilities.Standard}"),
                    new SortParam<bool>(SortParam.Sort48.PropertyName, false),
                    new SortParam<string>(SortParam.DirectResidueOrder.PropertyName, $"{USEParams.DirectResidueOrders.AlternateDirectResidue}"),
                    new SortParam<bool>(SortParam.AllowSelectionTrayWithdrawal.PropertyName, false),
                    new SortParam<bool>(SortParam.AllowAutoDpsLevelAdjustment.PropertyName, false),
                    new SortParam<bool>(SortParam.SwitchOffVolumeChecking.PropertyName, false),
                    new SortParam<bool>(SortParam.DsaMachinable.PropertyName, false),
        }
    };

    if (Directory.Exists(testDirectory))
        Directory.Delete(testDirectory, true);

    var sort = UseFileClient(currentDirectory);
    var sortResult = sort.RunSort(sortDefinition, DisplayStatusMessage, false);

    DisplayStatusMessage(new object(), $"Completed sort 935Citipost with result {sortResult.Ok} {sortResult.Message}");
}
